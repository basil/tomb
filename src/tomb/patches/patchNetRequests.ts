import * as spitroast from 'spitroast';
import { fromK9A, has } from '../util.ts';

// If needed, change the URL
function patchURL(originalURL: string) {
	// Fixes two network errors that happen because we use a different index.html
	if (originalURL === '') return 'index.html';

	let url = fromK9A(decodeURI(originalURL));

	if (!$tomb.cache.isAssetModified(url)) return originalURL;

	let patched = false;

	// Data deltas
	if (has($tomb.patches.delta.data, url)) {
		url = $tomb.cache.getDeltaData(url);
		patched = true;
	}

	// Image deltas
	if (has($tomb.patches.delta.image, url)) {
		url = $tomb.cache.getDeltaImage(url);
		patched = true;
	}

	// Static assets
	if (has($tomb.patches.static.assets, url)) {
		// Handle conflicts
		// TODO: Move this to `processMods` instead of figuring it out at runtime
		if (patched)
			throw new Error(
				`Attempting to modify ${url}, but it has already been delta modified.`
			);

		url = $tomb.cache.getStaticAsset(url);
	}

	return url;
}

export default function patchNetRequests() {
	// Most assets
	spitroast.before(
		'open',
		XMLHttpRequest.prototype,
		(args: [string, string]) => {
			args[1] = patchURL(args[1]);

			return args;
		}
	);

	// Only used for fetching audio
	spitroast.before('fetch', window, (args) => {
		args[0] = patchURL(args[0]);

		return args;
	});
}
