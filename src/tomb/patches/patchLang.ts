import type { CoffinLang } from '../../types/Coffin.ts';

import path from 'path';
import { deepMerge } from '../util.ts';

function handleLang(
	[absFilePath]: [string],
	response: CoffinLang
): CoffinLang | undefined {
	const dirs = path.dirname(absFilePath).split(path.sep);
	const name = dirs[dirs.length - 1];

	const lang = $tomb.patches.delta.language[name];
	if (!lang) return;

	return deepMerge(response, lang) as CoffinLang;
}

export default function patchLang() {
	$tomb.lib.spitroast.before('run', SceneManager, () => {
		// Wrapped in functions so that they can access `this`.
		$tomb.lib.lang = {
			loadLOC: function (filePath) {
				return Lang.loadLOC(filePath);
			},
			loadTXT: function (filePath) {
				return Lang.loadTXT(filePath);
			},
			loadCSV: function (filePath) {
				return Lang.loadCSV(filePath);
			}
		};

		$tomb.lib.spitroast.after('loadLOC', Lang, handleLang);
		$tomb.lib.spitroast.after('loadTXT', Lang, handleLang);
		$tomb.lib.spitroast.after('loadCSV', Lang, handleLang);
	});
}
