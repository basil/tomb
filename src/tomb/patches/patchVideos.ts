import * as spitroast from 'spitroast';
import { fromK9A } from '../util.ts';

// If needed, change the URL
function patchURL(originalURL: string) {
	const url = fromK9A(decodeURI(originalURL));

	if (!$tomb.cache.isAssetModified(url)) return originalURL;

	// The only asset modification that could happen here is static assets
	return $tomb.cache.getStaticAsset(url);
}

export default function patchVideos() {
	spitroast.before(
		'_playVideo',
		Graphics,
		(args: [src: string]) => {
			args[0] = patchURL(args[0]);

			return args;
		}
	);
}
