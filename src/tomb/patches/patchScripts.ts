import * as spitroast from 'spitroast';
import { has } from '../util.ts';

export default function patchScripts() {
	// Engine patching
	spitroast.instead('loadScript', PluginManager, function ([name]: [string]) {
		let url;

		const noEnding = name.slice(0, -3);
		if (has($tomb.patches.static.plugin, noEnding))
			url = $tomb.cache.getStaticPlugin(noEnding);
		else url = this._path + name;

		const script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = url;
		script.async = false;
		script.onerror = this.onError.bind(this);
		// @ts-expect-error Not defined on HTMLScriptElement, but is set anyway
		script._url = url;
		document.body.appendChild(script);
	});
}
