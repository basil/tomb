import { fromK9A } from '../util.ts';

export default function patchDekit() {
	// @ts-expect-error Crypto is not yet typed
	if (Crypto.dekit) {
		$tomb.lib.spitroast.instead(
			'dekit',
			Crypto,
			function (
				args: [fileContent: unknown, filePath: string, guard: number],
				orig
			) {
				const [content, path] = args;
				if ($tomb.cache.isAssetModified(fromK9A(decodeURI(path)))) {
					return content;
				}

				return orig.apply(this, args);
			}
		);
	}
}
