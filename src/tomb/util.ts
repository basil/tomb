import type Mod from './classes/mod.ts';

// `hasOwn` is better but does not exist on the version of NWjs the game uses
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/hasOwn#browser_compatibility
export const has = (obj: unknown, property: string): boolean =>
	Object.prototype.hasOwnProperty.call(obj, property);

// TypeScript refuses to function and says this only returns `Mod` for some reason.
export const getModById = (id: string): Mod | undefined =>
	$tomb.mods.find(mod => mod.id === id);

/** Plurality helper. */
export const s = (num: number) => num === 1 ? '' : 's';

// https://stackoverflow.com/a/58618599
const isObject = (item: unknown) =>
	item && typeof item === 'object' && !Array.isArray(item);
export function deepMerge<T>(target: T, ...sources: T[]): T {
	if (!sources.length) return target;
	const source = sources.shift();

	if (isObject(target) && isObject(source)) {
		for (const key in source) {
			if (isObject(source[key])) {
				if (!target[key]) Object.assign(target, { [key]: {} });
				deepMerge(target[key], source[key]);
			} else {
				Object.assign(target, { [key]: source[key] });
			}
		}
	}

	return deepMerge(target, ...sources);
}

/** Converts from `.k9a` to the proper file extension. */
export const fromK9A = (fullPath: string) => {
	if (/^data(\/|\\)/.test(fullPath)) return fullPath.replace(/\.k9a$/, '.json');
	if (/^img(\/|\\)/.test(fullPath)) return fullPath.replace(/\.k9a$/, '.png');
	if (/^audio(\/|\\)/.test(fullPath)) return fullPath.replace(/\.k9a$/, '.ogg');
	return fullPath;
};

/** Converts from the proper file extension to `.k9a`. */
export const toK9A = (fullPath: string) =>
	fullPath.replace(/\.(png|ogg|json)$/, '.k9a');

/** Writes a message to the on-screen log. */
export const bootLog = (msg: string) => {
	const log = document.querySelector('#log') as HTMLPreElement | undefined;

	if (log) {
		log.innerText += msg + '\n';
		log.scrollTop = log.scrollHeight;
	}
};

/** Gets the version string of the game. */
export const gameVersion = () => GAME_VERSION ?? VERSION;
