import { satisfies } from 'semver';
import { gameVersion, getModById, s } from './util.ts';

export function checkSpec() {
	const errors: string[] = [];

	for (const mod of $tomb.mods) {
		const { spec } = mod.dependencies;

		if (!satisfies(spec, '0.1.0')) {
			errors.push(`Mod "${mod.name}" expects support for the mod.json spec version "${spec}", but this version of Tomb does not support it.`);
			continue;
		}
	}

	if (errors.length > 0)
		throw new Error(`Error${s(errors.length)} encountered:\n- ${errors.join('\n- ')}`);
}

export function checkMods() {
	const errors: string[] = [];

	for (const mod of $tomb.mods) {
		const { mods } = mod.dependencies;

		if (mods === undefined) continue;

		for (const [id, range] of Object.entries(mods)) {
			const depMod = getModById(id);

			if (!depMod) {
				errors.push(`Mod "${mod.name}" expects a mod with the ID of "${id}" to be installed, but no mod with that ID could be found.`);
				continue;
			}
			
			if (!satisfies(depMod.version, range)) {
				errors.push(`Mod "${mod.name}" expects a different version of the mod "${depMod.name}" | Expected range: "${range}" Recived: "${depMod.version}"`);
				continue;
			}
		}
	}

	if (errors.length > 0)
		throw new Error(`Error${s(errors.length)} encountered:\n- ${errors.join('\n- ')}`);
}

export function checkGame() {
	const errors: string[] = [];

	for (const mod of $tomb.mods) {
		const { game } = mod.dependencies;
			
		if (!satisfies(gameVersion(), game)) {
			errors.push(`Mod "${mod.name}" expects a different version of the game | Expected range: "${game}" Recived: "${gameVersion()}"`);
			continue;
		}
	}

	if (errors.length > 0)
		throw new Error(`Error${s(errors.length)} encountered:\n- ${errors.join('\n- ')}`);
}
