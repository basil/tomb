import type {
	AssetStatic,
	DataDelta,
	ImageDelta,
	LanguageDelta,
	PluginStatic
} from '../mods.ts';
import type { CoffinLang } from '../../types/Coffin.ts';
import type Mod from './mod.ts';

import fs from 'fs';
import path from 'path';
import * as spitroast from 'spitroast';
import { satisfies } from 'semver';
import Cache from './cache.ts';
import GUI from './gui.ts';
import { parseMods, processMods } from '../mods.ts';
import { getModById, has, bootLog, s } from '../util.ts';
import { checkGame, checkMods, checkSpec } from '../dependencies.ts';
import patchNetRequests from '../patches/patchNetRequests.ts';
import patchScripts from '../patches/patchScripts.ts';
import patchLang from '../patches/patchLang.ts';
import patchDekit from '../patches/patchDekit.ts';
import patchVideos from '../patches/patchVideos.ts';

export type Patches = {
	delta: {
		data: DataDelta;
		image: ImageDelta;
		language: LanguageDelta;
	};
	static: {
		plugin: PluginStatic;
		assets: AssetStatic;
	};
};

export type InjectionPoint =
	| 'tombPostModParse'
	| 'tombPostModProcess'
	| 'tombPostPluginInject';

export default class Tomb {
	readonly version: string | undefined;
	mods: Mod[];
	patches: Patches;
	readonly cache: Cache;
	readonly gui: GUI;
	readonly lib: {
		readonly spitroast: typeof spitroast;
		readonly semver: {
			readonly satisfies: typeof satisfies;
		};
		lang?: {
			loadLOC(filePath: string): CoffinLang;
			loadTXT(filePath: string): CoffinLang;
			loadCSV(filePath: string): CoffinLang;
		};
	};

	constructor() {
		if (has(window, '$tomb'))
			throw new Error(
				'Tomb has already been initialized ($tomb global already exists)'
			);
		window.$tomb = this;

		// Dynamically added by build script
		this.version = process.env.TOMB_VERSION;

		this.gui = new GUI();

		// Add an error handler early
		window.addEventListener('error', (e) => {
			bootLog('Error: ' + e.message);
			this.gui.showError(e.error, true);
		});

		this.cache = new Cache();
		this.lib = {
			spitroast,
			semver: { satisfies }
		};
	}

	async loadMods() {
		bootLog('Loading mods');

		this.mods = await parseMods();
		this.hook('tombPostModParse');
		checkSpec();
		checkMods();
		this.patches = processMods(this.mods);
		this.hook('tombPostModProcess');

		const count = this.mods.length;
		bootLog(`${count} mod${s(count)} loaded`);
	}

	init() {
		patchNetRequests();
	}

	// Taken from CALM (which I think took it from CCLoader?)
	// https://github.com/ac2pic/CALM/blob/6ebdfbf4e8fa89c1b40d8438f8d85bc7df193e10/calm/js/calm.js#L37
	loadGame() {
		bootLog('Loading the game');

		// Load the original document
		const indexSource = fs.readFileSync(path.join('www', 'index.html'), 'utf8');
		const index = new DOMParser().parseFromString(indexSource, 'text/html');

		// Redirect all requests
		// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base
		const base = index.createElement('base');
		base.href = '/www/';
		index.head.insertBefore(base, index.head.firstChild);

		// Re-add the log
		index.body.insertBefore(document.querySelector('#log'), index.body.firstChild);

		// Scripts
		const formatScript = (fn: () => void) => '(' + fn.toString() + ')();';

		// Insert the plugin hook so we can load our own plugins
		{
			const script = index.createElement('script');
			script.type = 'text/javascript';
			script.textContent = formatScript(() => $tomb.postPlugins());
			index.querySelector('script[src="js/plugins.js"]').after(script);
		}

		// Replace `window.onload` so that we have time to patch assets
		{
			const script = index.createElement('script');
			script.type = 'text/javascript';
			script.textContent = formatScript(() => $tomb.preWindowLoad());
			index.body.append(script);
		}

		// Replace the document
		document.open();
		document.write(index.documentElement.outerHTML);
		document.close();

		// Seems `require` uses this to resolve relative file paths
		process.mainModule.filename = path.join(
			process.mainModule.filename,
			'..',
			'..',
			'www',
			'index.html'
		);
	}

	hook(hook: InjectionPoint) {
		bootLog('Running hook ' + hook);

		const injections: string[] = [];

		DEV: {
			if (!this.mods)
				throw new Error(
					'Error: Attempting to run a hook before mods are parsed'
				);
		}

		// Get all of the files we need to run
		for (const mod of this.mods) {
			if (!mod.files.inject) continue;

			for (const injection of mod.files.inject) {
				if (injection.at === hook) {
					// We can't run checks in `processMods`, since by that point we've already triggered a hook, so we have to do them here

					// Make sure the file actually exists
					if (!mod.exists(injection.file))
						throw new Error(
							`Mod "${mod.name}" is trying to inject ${hook} hook, but the specified file "${injection.file}" does not exist`
						);

					injections.push(mod.readTextFile(injection.file));
				}
			}
		}

		for (const injection of injections) {
			// Potential other way to do this, but it requires `module.exports` boilerplate
			// require(`./mods/${mod.path}/${script}.js`)(this);

			// This is effectively the same as `eval`, just gets around an esbuild warning
			// https://esbuild.github.io/content-types/#direct-eval
			const fn = new Function('$tomb', injection);
			fn(this);
		}
	}

	postPlugins() {
		for (const key of Object.keys(this.patches.static.plugin)) {
			const modId = this.patches.static.plugin[key];
			const mod = getModById(modId);

			window.$plugins.push({
				name: key,
				// Whether or not the plugin is enabled
				status: true,
				description: `A plugin injected by Tomb from the mod "${mod.name}"`,
				parameters: {}
			});
		}

		this.hook('tombPostPluginInject');

		patchScripts();
		patchLang();
		patchVideos();
	}

	preWindowLoad() {
		const orig = window.onload;
		window.onload = async function (...args) {
			try { checkGame(); }
			catch (e) {
				$tomb.gui.showError(e);
				throw e;
			}

			patchDekit();

			await $tomb.cache.generatePatchedImages();

			document.querySelector('#log').remove();

			orig.apply(this, args);
		};
	}
}
