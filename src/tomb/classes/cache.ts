import fs from 'fs';
import path from 'path';
import zlib from 'zlib';
import { applyPatch } from 'fast-json-patch';
import { applyDiffs } from 'olid.ts';
import { bootLog, getModById, has, s } from '../util.ts';
import decrypt from '../../mods/tomb/plugins/lib/util/decrypt.ts';

export default class Cache {
	private cache: {
		delta: {
			data: Record<string, string>;
			image: Record<string, Buffer>;
		};
		static: {
			asset: Record<string, string>;
			plugin: Record<string, string>;
		};
	};

	constructor() {
		this.cache = {
			delta: {
				data: {},
				image: {}
			},
			static: {
				asset: {},
				plugin: {}
			}
		};
	}

	private getOriginalFile(filePath: string): Uint8Array {
		let buf, newFilePath;
		try {
			// 2.0.10+
			newFilePath = filePath.replace(/\.(png|ogg|json)$/, '.k9a');
			buf = fs.readFileSync(newFilePath);
		} catch {
			// 2.0.9 and below
			newFilePath = filePath;
			buf = fs.readFileSync(newFilePath);
		}

		const decrypted = decrypt(new Uint8Array(buf), newFilePath);
		return decrypted;
	}

	getStaticAsset(filePath: string) {
		if (!has(this.cache.static.asset, filePath)) {
			const modId = $tomb.patches.static.assets[filePath];
			const mod = getModById(modId);

			const data = mod.readFile(filePath);

			this.cache.static.asset[filePath] = URL.createObjectURL(new Blob([data]));
		}

		return this.cache.static.asset[filePath];
	}

	getStaticPlugin(filePath: string) {
		if (!has(this.cache.static.plugin, filePath)) {
			const modId = $tomb.patches.static.plugin[filePath];
			const mod = getModById(modId);

			const data = mod.readTextFile(filePath.replace(/^.+?\//, ''));

			this.cache.static.plugin[filePath] = URL.createObjectURL(
				new Blob([data])
			);
		}

		return this.cache.static.plugin[filePath];
	}

	getDeltaData(filePath: string) {
		if (!has(this.cache.delta.data, filePath)) {
			// Alternatively, we could also store the mod IDs instead of the raw patches, and extract the patches from said mods here
			// I don't think there's much of a benefit to doing that though, other than parity with `getImageData`
			const patches = $tomb.patches.delta.data[filePath];

			const decrypted = this.getOriginalFile(path.join('www', filePath));
			const str = new TextDecoder().decode(decrypted);
			const patched = applyPatch(JSON.parse(str), patches).newDocument;

			this.cache.delta.data[filePath] = URL.createObjectURL(
				new Blob([JSON.stringify(patched)])
			);
		}

		return this.cache.delta.data[filePath];
	}

	async generatePatchedImages() {
		const imageCount = Object.keys($tomb.patches.delta.image).length;
		bootLog(`Patching ${imageCount} image${s(imageCount)}`);

		DEV: console.time(`🪦 Patching images`);

		const _padLength = imageCount.toString().length;
		const pad = (i: number) => i
			.toString()
			.padStart(_padLength, '0');

		// I'm not sure, but I feel like doing this asynchronously with `Promise.all` might be a bad idea,
		// given that it would probably put the computer under a lot of load
		let i = 1;
		for (const [filePath, modIds] of Object.entries(
			$tomb.patches.delta.image
		)) {
			bootLog(`Patching image [${pad(i++)}/${imageCount}] ${filePath}`);
			DEV: console.time(`🪦 Patching ${filePath}`);

			const decrypted = this.getOriginalFile(path.join('www', filePath));
			const img = await createImageBitmap(new Blob([decrypted]));

			// Parse all of the delta files
			const deltas = modIds.map((modId) => {
				// Simply using .buffer doesn't seem to work here

				const buffer = getModById(modId).readFile(filePath + '.olid');
				const arrayBuffer = new ArrayBuffer(buffer.length);
				const view = new Uint8Array(arrayBuffer);
				for (let i = 0; i < buffer.length; ++i) {
					view[i] = buffer[i];
				}
				return arrayBuffer;
			});

			// The PNG dataURL
			const data = applyDiffs(img, deltas);

			// Store the dataURL in compressed form
			this.cache.delta.image[filePath] = zlib.deflateSync(data);

			DEV: console.timeEnd(`🪦 Patching ${filePath}`);
		}

		DEV: console.timeEnd(`🪦 Patching images`);
	}

	getDeltaImage(filePath: string): string {
		return zlib.inflateSync(this.cache.delta.image[filePath]).toString();
	}

	isAssetModified(filePath: string): boolean {
		return (
			has($tomb.patches.delta.data, filePath) ||
			has($tomb.patches.delta.image, filePath) ||
			has($tomb.patches.static.assets, filePath) ||
			has($tomb.patches.static.plugin, filePath)
		);
	}
}
