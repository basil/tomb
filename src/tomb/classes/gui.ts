export default class GUI {
	/**
	 * Utility function to create and style an HTML element.
	 * @param node HTML element, like 'p' or 'div'
	 * @param styles An
	 * @returns The created element
	 */
	e<K extends keyof HTMLElementTagNameMap>(
		node: K,
		styles: Partial<CSSStyleDeclaration> = {}
	): HTMLElementTagNameMap[K] {
		const el = document.createElement(node);
		for (const [key, val] of Object.entries(styles)) {
			// @ts-expect-error Errors because some variables are read-only, like length. We're not going to set those, so it's fine
			el.style[key] = val;
		}
		return el;
	}

	showError(error: Error, exit: boolean = false) {
		const { e } = this;

		// Main container
		const container = e('div', {
			zIndex: '100',
			position: 'absolute',
			top: '40px',
			left: '50%',
			width: '400px',
			transform: 'translateX(-50%)',
			padding: '12px 8px',
			border: '1px solid #931b2b',
			borderRadius: '4px',
			background: 'radial-gradient(circle at top, #480e15 25%, #3e070d)',
			color: 'white',
			font: 'inherit',
			fontSize: 'inherit'
		});

		const title = e('h1', {
			textAlign: 'center',
			margin: '0',
			marginBottom: '12px',
			fontSize: '1.5rem',
			textShadow: '0 3px 2px #200306'
		});
		title.innerText = 'An error occurred';
		container.append(title);

		// The container for the message and stack trace
		const errorContainer = e('div', {
			padding: '8px',
			borderRadius: '4px',
			backgroundColor: '#1a0000',
			boxShadow: '0 4px 8px -2px #24010f, inset 0 0 16px #140000'
		});

		// Error message
		if (error.message) {
			for (const line of error.message.split('\n')) {
				const message = e('p', {
					whiteSpace: 'pre-wrap',
					fontFamily: 'monospace',
					margin: '0',
					lineHeight: '1.25',
					marginBottom: '8px'
				});
				message.innerText = line;
				errorContainer.append(message);
			}
		}

		// The stack trace
		const stackContainer = e('div', {
			overflow: 'auto',
			marginTop: '16px'
		});
		stackContainer.id = 'tomb-stack';

		// We can only style the scrollbar through a method like this
		const style = e('style');
		style.innerText = `
			#tomb-stack::-webkit-scrollbar {
				background-color: transparent;
				height: 8px;
			}

			#tomb-stack::-webkit-scrollbar-thumb {
				background-color: #650e17;
				border: 1px solid #76101b;
				border-radius: 4px;
			}
		`;
		document.head.appendChild(style);

		const lines = error.stack
			.slice(`${error.name}: ${error.message}`.length)
			.split('\n')
			.slice(1);
		for (const line of lines) {
			// The function that caused the error (if applicable) and the file path
			const [fn, file] = (() => {
				const fnAndFile = line.match(/^\s+at (.+) \((.+?)\)$/);
				if (fnAndFile) return [fnAndFile[1], fnAndFile[2]];

				const file = line.match(/^\s+at (.+?)$/);
				if (file) return [undefined, file[1]];

				console.error('Tomb Error GUI: Failed to parse line: ' + line);
				return [undefined, ''];
			})();

			const lineContainer = e('span', {
				display: 'block',
				marginLeft: '2ch',
				fontFamily: 'monospace',
				fontSize: '0.75rem',
				lineHeight: '1.25',
				whiteSpace: 'pre'
			});

			// If the function exists, add it
			if (fn) {
				const fnLine = e('span', {
					fontWeight: '600',
					whiteSpace: 'pre'
				});
				// The added space serves both to add spacing and for formatting, if someone copy-pastes the text.
				fnLine.innerText = fn + ' ';
				lineContainer.append(fnLine);
			}

			// The file is always present
			const source = e('span', {
				color: '#ea7777',
				whiteSpace: 'nowrap'
			});
			source.innerText = file.replace(/^chrome-extension:\/\/\w+\/www\//, '');
			lineContainer.append(source);

			stackContainer.append(lineContainer);
		}

		errorContainer.append(stackContainer);
		container.append(errorContainer);

		// Add buttons
		const buttons = e('div', {
			display: 'flex',
			justifyContent: 'center',
			gap: '8px',
			marginTop: '8px'
		});
		const buttonStyles = {
			padding: '8px 0px',
			width: '100%',
			borderRadius: '4px',
			backgroundColor: '#1a0000',
			border: 'unset',
			color: 'inherit',
			fontFamily: 'inherit',
			fontSize: 'inherit',
			fontWeight: '600',
			boxShadow: '0 4px 8px -2px #24010f, inset 0 0 16px #140000',
			cursor: 'pointer'
		};

		// Reloading the app
		const reload = e('button', buttonStyles);
		reload.innerText = 'Reload';
		reload.addEventListener('click', () => chrome.runtime.reload());
		buttons.append(reload);

		// If we're using the SDK version, add a button to open the Chromium dev tools
		if (process.versions['nw-flavor'] === 'sdk') {
			const openDevTools = e('button', buttonStyles);
			openDevTools.innerText = 'Open Dev Tools';
			openDevTools.addEventListener('click', () =>
				require('nw.gui').Window.get().showDevTools()
			);
			buttons.append(openDevTools);
		}

		// A button to close the modal
		const closeError = e('button', buttonStyles);
		closeError.innerText = 'Close';
		closeError.addEventListener('click', () => container.remove());
		buttons.append(closeError);

		container.append(buttons);

		document.body.append(container);
		// Make sure no other scripts run
		if (exit) window.stop();
	}
}
