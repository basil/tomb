import type { InjectionPoint } from './tomb.ts';

import fs from 'fs';
import path from 'path';
import StreamZip from 'node-stream-zip';

type ModId = string; // i.e. 'andrew-yuri'

enum ModType {
	Folder = 'folder',
	Zip = 'zip'
}

export type ModJSON = {
	id: ModId;
	name: string;
	authors: string[];
	description: string;
	version: string;
	dependencies: ModDependencies;
	files: ModFiles;
};
type ModDependencies = {
	game: string;
	spec: string;
	mods?: Record<string, string>;
};
type ModFiles = Partial<{
	plugins: string[];
	dataDeltas: string[];
	imageDeltas: string[];
	assets: string[];
	languages: string[];
	inject: {
		file: string;
		at: InjectionPoint;
	}[];
}>;

export const modsFolder = path.join('tomb', 'mods');

export default class Mod {
	readonly type: ModType; // ZIP support may be added in the future is added
	readonly path: string; // Either the name of the folder, or in the future, the name (without the extension) of the ZIP file
	readonly zip?: StreamZip;

	readonly id: ModId;
	readonly name: string;
	readonly authors: string[];
	readonly description: string;
	readonly version: string;
	readonly dependencies: ModDependencies;
	readonly files: ModFiles;

	private constructor(
		type: ModType,
		path: string,
		json: ModJSON,
		zip?: StreamZip
	) {
		// Temporary until all current mods are update
		if (json.dependencies === undefined) throw new Error(`Mod "${json.name}" is missing the \`dependencies\` key. Download a newer version of this mod or ask the developer to update it.`);

		this.type = type;
		this.path = path;
		if (zip) this.zip = zip;

		this.id = json.id;
		this.name = json.name;
		this.authors = json.authors;
		this.description = json.description;
		this.version = json.version;
		this.dependencies = json.dependencies;
		this.files = json.files;
	}

	static init(pathInMods: string) {
		return new Promise<Mod>((resolve, reject) => {
			// pathInMods will either be something like 'mod.zip' or 'mod/' (a folder path)
			const modPath = path.join(modsFolder, pathInMods);

			const stat = fs.statSync(path.join(modsFolder, pathInMods));

			if (stat.isDirectory()) {
				const modJsonPath = path.join(modPath, 'mod.json');
				if (!fs.existsSync(modJsonPath)) {
					throw new Error(
						`Mod at location ${pathInMods} does not have a mod.json at the expected location, ${modJsonPath}`
					);
				}

				const rawJson = fs.readFileSync(modJsonPath, 'utf8');
				const modJson: ModJSON = JSON.parse(rawJson);
				const mod = new Mod(ModType.Folder, pathInMods, modJson);

				resolve(mod);
			} else if (stat.isFile()) {
				// This should be closed (`zip.close()`), but presumably Node.js closes them automatically
				const zip = new StreamZip({
					file: modPath
				});

				zip.on('ready', () => {
					let buffer;
					try {
						buffer = zip.entryDataSync('mod.json');
					} catch {
						throw new Error(
							`Could not read mod.json in ${pathInMods}. Make sure mod.json is at the root of the ZIP.`
						);
					}

					const modJson: ModJSON = JSON.parse(buffer.toString());
					try {
						const mod = new Mod(ModType.Zip, pathInMods, modJson, zip);
						resolve(mod);
					} catch (e) {
						reject(e);
					}

				});
			} else {
				throw new Error('Other mod format not supported');
			}
		});
	}

	exists(pathToFile: string) {
		if (this.type === ModType.Folder)
			return fs.existsSync(path.join(modsFolder, this.path, pathToFile));

		if (this.type === ModType.Zip)
			return this.zip!.entries()[pathToFile] !== undefined;
	}

	readFile(pathToFile: string) {
		if (this.type === ModType.Folder)
			return fs.readFileSync(path.join(modsFolder, this.path, pathToFile));

		if (this.type === ModType.Zip) return this.zip!.entryDataSync(pathToFile);
	}

	readTextFile(pathToFile: string) {
		if (this.type === ModType.Folder)
			return fs.readFileSync(
				path.join(modsFolder, this.path, pathToFile),
				'utf8'
			);

		if (this.type === ModType.Zip)
			return this.zip!.entryDataSync(pathToFile).toString();
	}
}
