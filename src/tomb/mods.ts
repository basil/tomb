import type { Operation as JsonPatchOperation } from 'fast-json-patch';
import type { Patches } from './classes/tomb.ts';
import type { CoffinLang } from '../types/Coffin.ts';

import fs from 'fs';
import path from 'path';
import Mod, { modsFolder } from './classes/mod.ts';
import { getModById, has } from './util.ts';

type FilePath = string; // i.e. 'audio/bgm/brain_power_noma.ogg'
type ModId = string; // i.e. 'andrew-yuri'
export type LangData = Partial<
	Pick<CoffinLang, 'sysLabel' | 'sysMenus' | 'labelLUT' | 'linesLUT'>
>;

export type DataDelta = Record<FilePath, JsonPatchOperation[]>;
export type ImageDelta = Record<FilePath, ModId[]>;
export type PluginStatic = Record<FilePath, ModId>;
export type AssetStatic = Record<FilePath, ModId>;
export type LanguageDelta = Record<string, LangData>;

/**
 * Parses all `mod.json` files.
 */
export async function parseMods(): Promise<Mod[]> {
	const mods: Mod[] = [];

	for (const entry of fs.readdirSync(modsFolder)) {
		const stat = fs.statSync(path.join(modsFolder, entry));
		if (!stat.isDirectory() && !entry.toLowerCase().endsWith('.zip')) continue;

		const mod = await Mod.init(entry);

		if (!/^[0-9a-z-_]{1,}$/.test(mod.id))
			throw new Error(
				`Mod ID Error: Mod "${mod.name}" has ID "${mod.id}" but mod IDs must only consist of lowercase letters, numbers, and the following characters: '-' and '_'`
			);

		const conflict = mods.find(({ id }) => id === mod.id);
		if (conflict)
			throw new Error(
				`Mod ID Conflict: Mod "${mod.name}" has ID "${mod.id}" which conflicts with mod "${conflict.name}"`
			);

		mods.push(mod);
	}

	return mods;
}

/**
 * Compare.
 */
export function processMods(mods: Mod[]): Patches {
	const dataDelta: DataDelta = {};
	const imageDelta: ImageDelta = {};
	const pluginStatic: PluginStatic = {};
	const assetStatic: AssetStatic = {};
	const lanuageDelta: LanguageDelta = {};

	// Used for checking conflicts in language files
	const prevLanguages: Record<
		string,
		{
			modName: string;
			lang: LangData;
		}[]
	> = {};

	// Assemble data
	for (const mod of mods) {
		if (mod.files.dataDeltas) {
			// Data files are .JSON files stored in a flat directory structure
			for (let deltaPath of mod.files.dataDeltas) {
				if (!mod.exists(deltaPath))
					throw new Error(
						`Mod "${mod.name}" missing JSON delta patch at path: ${deltaPath}`
					);

				const delta = JSON.parse(mod.readTextFile(deltaPath));

				// We add it to the index as a `.json` file so we can match the URL
				deltaPath = deltaPath.replace(/\.jsond$/, '.json');

				// Add to the index
				if (has(dataDelta, deltaPath))
					dataDelta[deltaPath] = [...dataDelta[deltaPath], ...delta];
				else dataDelta[deltaPath] = delta;
			}
		}

		if (mod.files.imageDeltas) {
			// Data files are .JSON files stored in a flat directory structure
			for (let deltaPath of mod.files.imageDeltas) {
				if (!mod.exists(deltaPath))
					throw new Error(
						`Mod "${mod.name}" missing image delta patch at path: ${deltaPath}`
					);

				// Again, remove our extra extension so we can match it during network requests
				deltaPath = deltaPath.replace(/\.olid$/, '');

				//! NOTE: I'm not sure what the expected behavior for two patches that influence the same area is

				// Add to the index
				if (has(imageDelta, deltaPath))
					imageDelta[deltaPath] = [...imageDelta[deltaPath], mod.id];
				else imageDelta[deltaPath] = [mod.id];
			}
		}

		if (mod.files.plugins) {
			for (const pluginPath of mod.files.plugins) {
				if (!mod.exists(pluginPath))
					throw new Error(
						`Mod "${mod.name}" missing plugin at path: ${pluginPath}`
					);

				// Make sure it wouldn't conflict with an existing game plugin
				if (fs.existsSync(path.join('www', 'js', 'plugins', pluginPath)))
					throw new Error(
						`Conflict: Mod "${mod.name}" is trying to inject "${pluginPath}", but the file path conflicts with an existing game plugin at "www/js/plugins/${pluginPath}"\nMod developers: Consider moving the injected file to a subfolder or renaming it.`
					);

				// This is fine, because mods can't have slashes in their IDs
				pluginStatic[mod.id + '/' + pluginPath] = mod.id;
			}
		}

		if (mod.files.assets) {
			for (const assetPath of mod.files.assets) {
				if (!mod.exists(assetPath))
					throw new Error(
						`Mod "${mod.name}" missing asset at path: ${assetPath}`
					);

				if (has(assetStatic, assetPath)) {
					const conflictingMod = getModById(assetStatic[assetPath]);
					throw new Error(
						`Conflict: Plugin "${mod.name}" is trying to override ${assetPath}, but it's already overridden by "${conflictingMod.name}"`
					);
				}

				assetStatic[assetPath] = mod.id;
			}
		}

		if (mod.files.languages) {
			for (const languagePath of mod.files.languages) {
				if (!mod.exists(languagePath))
					throw new Error(
						`Mod "${mod.name}" missing language file at path: ${languagePath}`
					);

				let lang;
				try {
					lang = JSON.parse(mod.readTextFile(languagePath)) as LangData;
				} catch {
					throw new Error(
						`Error: Failed to read file ${languagePath} from mod "${mod.name}"`
					);
				}

				// Name of the language
				const name = path.basename(languagePath, '.json');

				for (const langKey of [
					'sysLabel',
					'sysMenus',
					'labelLUT',
					'linesLUT'
				] as (keyof LangData)[]) {
					if (lang[langKey]) {
						if (!lanuageDelta[name]) lanuageDelta[name] = {};
						if (!lanuageDelta[name][langKey]) lanuageDelta[name][langKey] = {};

						for (const [key, val] of Object.entries(lang[langKey])) {
							// Make sure this key doesn't conflict with a previous language
							if (lanuageDelta[name][langKey][key]) {
								for (const { modName, lang } of prevLanguages[name]) {
									if (lang[langKey] && lang[langKey][key])
										throw new Error(
											`Conflict: Mod ${mod.name} trying to set "${langKey}[${key}]" but it has already been set by mod "${modName}"`
										);
								}
							}

							lanuageDelta[name][langKey][key] = val;
						}
					}
				}

				// This is just used for conflict checking
				if (prevLanguages[name])
					prevLanguages[name].push({ modName: mod.name, lang });
				else prevLanguages[name] = [{ modName: mod.name, lang }];
			}
		}
	}

	for (const asset of Object.keys(assetStatic)) {
		if (dataDelta[asset] || imageDelta[asset]) {
			throw new Error(
				`Conflict: File ${asset} cannot be both asset replaced and delta modified.`
			);
		}
	}

	return {
		delta: {
			data: dataDelta,
			image: imageDelta,
			language: lanuageDelta
		},
		static: {
			plugin: pluginStatic,
			assets: assetStatic
		}
	};
}
