import fs from 'fs';
import path from 'path';
import Tomb from './classes/tomb.ts';
import { bootLog } from './util.ts';

// This syntax looks really weird, but is valid and works with the build script!
// https://esbuild.github.io/api/#drop-labels
// This is removed during production builds by esbuild
DEV: {
	// This is triggered twice in the startup, and I'm not exactly sure why. It may be to do with the ESLint plugin

	const src = document.currentScript.getAttribute('src');
	const targets = [src, 'index.html'];
	for (const target of targets) {
		fs.watch(path.join(process.cwd(), 'tomb', target), () =>
			chrome.runtime.reload()
		);
	}
}

(async () => {
	try {
		bootLog(process.env.TOMB_VERSION_FORMATTED);
		new Tomb();
		await $tomb.loadMods();

		$tomb.init();
		$tomb.loadGame();
	} catch (e) {
		console.error(e);
		$tomb.gui.showError(e);
	}
})();
