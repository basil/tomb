export type CoffinLang = {
	langName: string;
	langInfo: [string, string, string];
	fontFace: string;
	fontSize: number;
	sysLabel: Record<string, string>;
	sysMenus: Record<string, string>;
	labelLUT: Record<string, string>;
	linesLUT: Record<string, string[]>;
};
