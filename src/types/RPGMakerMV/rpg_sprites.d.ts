declare function Sprite_Base(...args: any[]): void;
declare class Sprite_Base {
	constructor(...args: any[]);
	initialize(): void;
	_animationSprites: any[];
	_effectTarget: this;
	_hiding: boolean;
	update(): void;
	hide(): void;
	show(): void;
	updateVisibility(): void;
	visible: boolean;
	updateAnimationSprites(): void;
	startAnimation(animation: any, mirror: any, delay: any): void;
	isAnimationPlaying(): boolean;
}
declare function Sprite_Button(...args: any[]): void;
declare class Sprite_Button {
	constructor(...args: any[]);
	initialize(): void;
	_touching: boolean;
	_coldFrame: any;
	_hotFrame: any;
	_clickHandler: any;
	update(): void;
	updateFrame(): void;
	setColdFrame(x: any, y: any, width: any, height: any): void;
	setHotFrame(x: any, y: any, width: any, height: any): void;
	setClickHandler(method: any): void;
	callClickHandler(): void;
	processTouch(): void;
	isActive(): boolean;
	isButtonTouched(): boolean;
	canvasToLocalX(x: any): any;
	canvasToLocalY(y: any): any;
}
declare function Sprite_Character(...args: any[]): void;
declare class Sprite_Character {
	constructor(...args: any[]);
	initialize(character: any): void;
	initMembers(): void;
	_character: any;
	_balloonDuration: number;
	_tilesetId: any;
	_upperBody: any;
	_lowerBody: any;
	setCharacter(character: any): void;
	update(): void;
	updateVisibility(): void;
	visible: boolean;
	isTile(): boolean;
	tilesetBitmap(tileId: any): any;
	updateBitmap(): void;
	_tileId: any;
	_characterName: any;
	_characterIndex: any;
	isImageChanged(): boolean;
	setTileBitmap(): void;
	bitmap: any;
	setCharacterBitmap(): void;
	_isBigCharacter: any;
	updateFrame(): void;
	updateTileFrame(): void;
	updateCharacterFrame(): void;
	characterBlockX(): number;
	characterBlockY(): number;
	characterPatternX(): any;
	characterPatternY(): number;
	patternWidth(): any;
	patternHeight(): any;
	updateHalfBodySprites(): void;
	createHalfBodySprites(): void;
	updatePosition(): void;
	x: any;
	y: any;
	z: any;
	updateAnimation(): void;
	updateOther(): void;
	opacity: any;
	blendMode: any;
	_bushDepth: any;
	setupAnimation(): void;
	setupBalloon(): void;
	startBalloon(): void;
	_balloonSprite: Sprite_Balloon;
	updateBalloon(): void;
	endBalloon(): void;
	isBalloonPlaying(): boolean;
}
declare function Sprite_Battler(...args: any[]): void;
declare class Sprite_Battler {
	constructor(...args: any[]);
	initialize(battler: any): void;
	initMembers(): void;
	_battler: any;
	_damages: any[];
	_homeX: any;
	_homeY: any;
	_offsetX: any;
	_offsetY: any;
	_targetOffsetX: any;
	_targetOffsetY: any;
	_movementDuration: any;
	_selectionEffectCount: number;
	setBattler(battler: any): void;
	setHome(x: any, y: any): void;
	update(): void;
	bitmap: any;
	updateVisibility(): void;
	visible: boolean;
	updateMain(): void;
	updateBitmap(): void;
	updateFrame(): void;
	updateMove(): void;
	updatePosition(): void;
	x: any;
	y: any;
	updateAnimation(): void;
	updateDamagePopup(): void;
	updateSelectionEffect(): void;
	setupAnimation(): void;
	setupDamagePopup(): void;
	damageOffsetX(): number;
	damageOffsetY(): number;
	startMove(x: any, y: any, duration: any): void;
	onMoveEnd(): void;
	isEffecting(): boolean;
	isMoving(): boolean;
	inHomePosition(): boolean;
}
declare function Sprite_Actor(...args: any[]): void;
declare class Sprite_Actor {
	constructor(...args: any[]);
	initialize(battler: any): void;
	initMembers(): void;
	_battlerName: any;
	_motion: any;
	_motionCount: number;
	_pattern: any;
	createMainSprite(): void;
	_mainSprite: Sprite_Base;
	_effectTarget: Sprite_Base;
	createShadowSprite(): void;
	_shadowSprite: any;
	createWeaponSprite(): void;
	_weaponSprite: Sprite_Weapon;
	createStateSprite(): void;
	_stateSprite: Sprite_StateOverlay;
	setBattler(battler: any): void;
	_actor: any;
	moveToStartPosition(): void;
	setActorHome(index: any): void;
	update(): void;
	updateShadow(): void;
	updateMain(): void;
	setupMotion(): void;
	setupWeaponAnimation(): void;
	startMotion(motionType: any): void;
	updateTargetPosition(): void;
	updateBitmap(): void;
	updateFrame(): void;
	updateMove(): void;
	updateMotion(): void;
	updateMotionCount(): void;
	motionSpeed(): number;
	refreshMotion(): void;
	startEntryMotion(): void;
	stepForward(): void;
	stepBack(): void;
	retreat(): void;
	onMoveEnd(): void;
	damageOffsetX(): number;
	damageOffsetY(): number;
}
declare namespace Sprite_Actor {
	namespace MOTIONS {
		namespace walk {
			let index: number;
			let loop: boolean;
		}
		namespace wait {
			let index_1: number;
			export { index_1 as index };
			let loop_1: boolean;
			export { loop_1 as loop };
		}
		namespace chant {
			let index_2: number;
			export { index_2 as index };
			let loop_2: boolean;
			export { loop_2 as loop };
		}
		namespace guard {
			let index_3: number;
			export { index_3 as index };
			let loop_3: boolean;
			export { loop_3 as loop };
		}
		namespace damage {
			let index_4: number;
			export { index_4 as index };
			let loop_4: boolean;
			export { loop_4 as loop };
		}
		namespace evade {
			let index_5: number;
			export { index_5 as index };
			let loop_5: boolean;
			export { loop_5 as loop };
		}
		namespace thrust {
			let index_6: number;
			export { index_6 as index };
			let loop_6: boolean;
			export { loop_6 as loop };
		}
		namespace swing {
			let index_7: number;
			export { index_7 as index };
			let loop_7: boolean;
			export { loop_7 as loop };
		}
		namespace missile {
			let index_8: number;
			export { index_8 as index };
			let loop_8: boolean;
			export { loop_8 as loop };
		}
		namespace skill {
			let index_9: number;
			export { index_9 as index };
			let loop_9: boolean;
			export { loop_9 as loop };
		}
		namespace spell {
			let index_10: number;
			export { index_10 as index };
			let loop_10: boolean;
			export { loop_10 as loop };
		}
		namespace item {
			let index_11: number;
			export { index_11 as index };
			let loop_11: boolean;
			export { loop_11 as loop };
		}
		namespace escape {
			let index_12: number;
			export { index_12 as index };
			let loop_12: boolean;
			export { loop_12 as loop };
		}
		namespace victory {
			let index_13: number;
			export { index_13 as index };
			let loop_13: boolean;
			export { loop_13 as loop };
		}
		namespace dying {
			let index_14: number;
			export { index_14 as index };
			let loop_14: boolean;
			export { loop_14 as loop };
		}
		namespace abnormal {
			let index_15: number;
			export { index_15 as index };
			let loop_15: boolean;
			export { loop_15 as loop };
		}
		namespace sleep {
			let index_16: number;
			export { index_16 as index };
			let loop_16: boolean;
			export { loop_16 as loop };
		}
		namespace dead {
			let index_17: number;
			export { index_17 as index };
			let loop_17: boolean;
			export { loop_17 as loop };
		}
	}
}
declare function Sprite_Enemy(...args: any[]): void;
declare class Sprite_Enemy {
	constructor(...args: any[]);
	initialize(battler: any): void;
	initMembers(): void;
	_enemy: any;
	_appeared: any;
	_battlerName: any;
	_battlerHue: any;
	_effectType: any;
	_effectDuration: any;
	_shake: number;
	createStateIconSprite(): void;
	_stateIconSprite: Sprite_StateIcon;
	setBattler(battler: any): void;
	update(): void;
	updateBitmap(): void;
	loadBitmap(name: any, hue: any): void;
	bitmap: any;
	updateFrame(): void;
	updatePosition(): void;
	updateStateSprite(): void;
	initVisibility(): void;
	opacity: number;
	setupEffect(): void;
	startEffect(effectType: any): void;
	startAppear(): void;
	startDisappear(): void;
	startWhiten(): void;
	startBlink(): void;
	startCollapse(): void;
	startBossCollapse(): void;
	startInstantCollapse(): void;
	updateEffect(): void;
	isEffecting(): boolean;
	revertToNormal(): void;
	blendMode: any;
	updateWhiten(): void;
	updateBlink(): void;
	updateAppear(): void;
	updateDisappear(): void;
	updateCollapse(): void;
	updateBossCollapse(): void;
	updateInstantCollapse(): void;
	damageOffsetX(): number;
	damageOffsetY(): number;
}
/**
 * The sprite for displaying an animation.
 */
declare class Sprite_Animation {
	constructor(...args: any[]);
	initialize(): void;
	_reduceArtifacts: boolean;
	initMembers(): void;
	_target: any;
	_animation: any;
	_mirror: any;
	_delay: any;
	_rate: number;
	_duration: number;
	_flashColor: any;
	_flashDuration: any;
	_screenFlashDuration: any;
	_hidingDuration: any;
	_bitmap1: any;
	_bitmap2: any;
	_cellSprites: any[];
	_screenFlashSprite: any;
	_duplicated: boolean;
	z: number;
	setup(target: any, animation: any, mirror: any, delay: any): void;
	remove(): void;
	setupRate(): void;
	setupDuration(): void;
	update(): void;
	updateFlash(): void;
	updateScreenFlash(): void;
	absoluteX(): number;
	absoluteY(): number;
	updateHiding(): void;
	isPlaying(): boolean;
	loadBitmaps(): void;
	isReady(): any;
	createSprites(): void;
	createCellSprites(): void;
	createScreenFlashSprite(): void;
	updateMain(): void;
	updatePosition(): void;
	x: any;
	y: any;
	updateFrame(): void;
	currentFrameIndex(): number;
	updateAllCellSprites(frame: any): void;
	updateCellSprite(sprite: any, cell: any): void;
	processTimingData(timing: any): void;
	startFlash(color: any, duration: any): void;
	startScreenFlash(color: any, duration: any): void;
	startHiding(duration: any): void;
}
declare namespace Sprite_Animation {
	let _checker1: {};
	let _checker2: {};
}
declare function Sprite_Damage(...args: any[]): void;
declare class Sprite_Damage {
	constructor(...args: any[]);
	initialize(): void;
	_duration: number;
	_flashColor: number[];
	_flashDuration: number;
	_damageBitmap: any;
	setup(target: any): void;
	setupCriticalEffect(): void;
	digitWidth(): number;
	digitHeight(): number;
	createMiss(): void;
	createDigits(baseRow: any, value: any): void;
	createChildSprite(): any;
	update(): void;
	updateChild(sprite: any): void;
	updateFlash(): void;
	updateOpacity(): void;
	opacity: number;
	isPlaying(): boolean;
}
declare function Sprite_StateIcon(...args: any[]): void;
declare class Sprite_StateIcon {
	constructor(...args: any[]);
	initialize(): void;
	initMembers(): void;
	_battler: any;
	_iconIndex: any;
	_animationCount: number;
	_animationIndex: number;
	loadBitmap(): void;
	bitmap: any;
	setup(battler: any): void;
	update(): void;
	animationWait(): number;
	updateIcon(): void;
	updateFrame(): void;
}
declare namespace Sprite_StateIcon {
	let _iconWidth: number;
	let _iconHeight: number;
}
declare function Sprite_StateOverlay(...args: any[]): void;
declare class Sprite_StateOverlay {
	constructor(...args: any[]);
	initialize(): void;
	initMembers(): void;
	_battler: any;
	_overlayIndex: any;
	_animationCount: number;
	_pattern: number;
	loadBitmap(): void;
	bitmap: any;
	setup(battler: any): void;
	update(): void;
	animationWait(): number;
	updatePattern(): void;
	updateFrame(): void;
}
declare function Sprite_Weapon(...args: any[]): void;
declare class Sprite_Weapon {
	constructor(...args: any[]);
	initialize(): void;
	initMembers(): void;
	_weaponImageId: any;
	_animationCount: number;
	_pattern: number;
	x: number;
	setup(weaponImageId: any): void;
	update(): void;
	animationWait(): number;
	updatePattern(): void;
	loadBitmap(): void;
	bitmap: any;
	updateFrame(): void;
	isPlaying(): boolean;
}
declare function Sprite_Balloon(...args: any[]): void;
declare class Sprite_Balloon {
	constructor(...args: any[]);
	initialize(): void;
	initMembers(): void;
	_balloonId: any;
	_duration: number;
	z: number;
	loadBitmap(): void;
	bitmap: any;
	setup(balloonId: any): void;
	update(): void;
	updateFrame(): void;
	speed(): number;
	waitTime(): number;
	frameIndex(): number;
	isPlaying(): boolean;
}
declare function Sprite_Picture(...args: any[]): void;
declare class Sprite_Picture {
	constructor(...args: any[]);
	initialize(pictureId: any): void;
	_pictureId: any;
	_pictureName: any;
	_isPicture: boolean;
	picture(): any;
	update(): void;
	updateBitmap(): void;
	visible: boolean;
	bitmap: any;
	updateOrigin(): void;
	updatePosition(): void;
	x: number;
	y: number;
	updateScale(): void;
	updateTone(): void;
	updateOther(): void;
	opacity: any;
	blendMode: any;
	rotation: number;
	loadBitmap(): void;
}
declare function Sprite_Timer(...args: any[]): void;
declare class Sprite_Timer {
	constructor(...args: any[]);
	initialize(): void;
	_seconds: any;
	createBitmap(): void;
	bitmap: any;
	update(): void;
	updateBitmap(): void;
	redraw(): void;
	timerText(): string;
	updatePosition(): void;
	x: number;
	y: number;
	updateVisibility(): void;
	visible: any;
}
declare function Sprite_Destination(...args: any[]): void;
declare class Sprite_Destination {
	constructor(...args: any[]);
	initialize(): void;
	_frameCount: number;
	update(): void;
	visible: boolean;
	createBitmap(): void;
	bitmap: any;
	blendMode: any;
	updatePosition(): void;
	x: number;
	y: number;
	updateAnimation(): void;
	opacity: number;
}
declare function Spriteset_Base(...args: any[]): void;
declare class Spriteset_Base {
	constructor(...args: any[]);
	initialize(): void;
	_tone: any;
	opaque: boolean;
	createLowerLayer(): void;
	createUpperLayer(): void;
	update(): void;
	createBaseSprite(): void;
	_baseSprite: any;
	_blackScreen: any;
	createToneChanger(): void;
	createWebGLToneChanger(): void;
	_toneFilter: any;
	createCanvasToneChanger(): void;
	_toneSprite: any;
	createPictures(): void;
	_pictureContainer: any;
	createTimer(): void;
	_timerSprite: Sprite_Timer;
	createScreenSprites(): void;
	_flashSprite: any;
	_fadeSprite: any;
	updateScreenSprites(): void;
	updateToneChanger(): void;
	updateWebGLToneChanger(): void;
	updateCanvasToneChanger(): void;
	updatePosition(): void;
	x: number;
	y: number;
}
declare function Spriteset_Map(...args: any[]): void;
declare class Spriteset_Map {
	constructor(...args: any[]);
	initialize(): void;
	createLowerLayer(): void;
	update(): void;
	hideCharacters(): void;
	createParallax(): void;
	_parallax: any;
	createTilemap(): void;
	_tilemap: any;
	loadTileset(): void;
	_tileset: any;
	createCharacters(): void;
	_characterSprites: any[];
	createShadow(): void;
	_shadowSprite: any;
	createDestination(): void;
	_destinationSprite: Sprite_Destination;
	createWeather(): void;
	_weather: any;
	updateTileset(): void;
	_canvasReAddParallax(): void;
	updateParallax(): void;
	_parallaxName: any;
	updateTilemap(): void;
	updateShadow(): void;
	updateWeather(): void;
}
declare function Spriteset_Battle(...args: any[]): void;
declare class Spriteset_Battle {
	constructor(...args: any[]);
	initialize(): void;
	_battlebackLocated: boolean;
	createLowerLayer(): void;
	createBackground(): void;
	_backgroundSprite: any;
	update(): void;
	createBattleField(): void;
	_battleField: any;
	createBattleback(): void;
	_back1Sprite: any;
	_back2Sprite: any;
	updateBattleback(): void;
	locateBattleback(): void;
	battleback1Bitmap(): any;
	battleback2Bitmap(): any;
	battleback1Name(): any;
	battleback2Name(): any;
	overworldBattleback1Name(): string;
	overworldBattleback2Name(): string;
	normalBattleback1Name(): string;
	normalBattleback2Name(): string;
	terrainBattleback1Name(
		type: any
	):
		| 'Wasteland'
		| 'DirtField'
		| 'Desert'
		| 'Lava1'
		| 'Lava2'
		| 'Snowfield'
		| 'Clouds'
		| 'PoisonSwamp';
	terrainBattleback2Name(
		type: any
	):
		| 'Wasteland'
		| 'Desert'
		| 'Snowfield'
		| 'Clouds'
		| 'PoisonSwamp'
		| 'Forest'
		| 'Cliff'
		| 'Lava';
	defaultBattleback1Name(): string;
	defaultBattleback2Name(): string;
	shipBattleback1Name(): string;
	shipBattleback2Name(): string;
	autotileType(z: any): any;
	createEnemies(): void;
	_enemySprites: Sprite_Enemy[];
	compareEnemySprite(a: any, b: any): number;
	createActors(): void;
	_actorSprites: any[];
	updateActors(): void;
	battlerSprites(): Sprite_Enemy[];
	isAnimationPlaying(): boolean;
	isEffecting(): boolean;
	isAnyoneMoving(): boolean;
	isBusy(): boolean;
}
