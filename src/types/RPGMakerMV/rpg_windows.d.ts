declare class Window_Base extends __RPG_Window {
	constructor(...args: any[]);
	initialize(x: number, y: number, width: number, height: number): void;
	_opening: boolean;
	_closing: boolean;
	_dimmerSprite?: Sprite;
	lineHeight(): number;
	standardFontFace():
		| 'SimHei, Heiti TC, sans-serif'
		| 'Dotum, AppleGothic, sans-serif'
		| 'GameFont';
	standardFontSize(): number;
	standardPadding(): number;
	textPadding(): number;
	standardBackOpacity(): number;
	loadWindowskin(): void;
	windowskin: any;
	updatePadding(): void;
	padding: number;
	updateBackOpacity(): void;
	backOpacity: number;
	contentsWidth(): number;
	contentsHeight(): number;
	fittingHeight(numLines: any): number;
	updateTone(): void;
	createContents(): void;
	contents: any;
	resetFontSettings(): void;
	resetTextColor(): void;
	update(): void;
	updateOpen(): void;
	updateClose(): void;
	open(): void;
	close(): void;
	isOpening(): boolean;
	isClosing(): boolean;
	show(): void;
	visible: boolean;
	hide(): void;
	activate(): void;
	active: boolean;
	deactivate(): void;
	textColor(n: any): any;
	normalColor(): any;
	systemColor(): any;
	crisisColor(): any;
	deathColor(): any;
	gaugeBackColor(): any;
	hpGaugeColor1(): any;
	hpGaugeColor2(): any;
	mpGaugeColor1(): any;
	mpGaugeColor2(): any;
	mpCostColor(): any;
	powerUpColor(): any;
	powerDownColor(): any;
	tpGaugeColor1(): any;
	tpGaugeColor2(): any;
	tpCostColor(): any;
	pendingColor(): any;
	translucentOpacity(): number;
	changeTextColor(color: any): void;
	changePaintOpacity(enabled: any): void;
	drawText(text: any, x: any, y: any, maxWidth: any, align: any): void;
	textWidth(text: any): any;
	drawTextEx(text: any, x: any, y: any): number;
	convertEscapeCharacters(text: any): any;
	actorName(n: any): any;
	partyMemberName(n: any): any;
	processCharacter(textState: any): void;
	processNormalCharacter(textState: any): void;
	processNewLine(textState: any): void;
	processNewPage(textState: any): void;
	obtainEscapeCode(textState: any): string;
	obtainEscapeParam(textState: any): number | '';
	processEscapeCharacter(code: any, textState: any): void;
	processDrawIcon(iconIndex: any, textState: any): void;
	makeFontBigger(): void;
	makeFontSmaller(): void;
	calcTextHeight(textState: any, all: any): number;
	drawIcon(iconIndex: any, x: any, y: any): void;
	drawFace(
		faceName: any,
		faceIndex: any,
		x: any,
		y: any,
		width: any,
		height: any
	): void;
	drawCharacter(characterName: any, characterIndex: any, x: any, y: any): void;
	drawGauge(
		x: any,
		y: any,
		width: any,
		rate: any,
		color1: any,
		color2: any
	): void;
	hpColor(actor: any): any;
	mpColor(actor: any): any;
	tpColor(actor: any): any;
	drawActorCharacter(actor: any, x: any, y: any): void;
	drawActorFace(actor: any, x: any, y: any, width: any, height: any): void;
	drawActorName(actor: any, x: any, y: any, width: any): void;
	drawActorClass(actor: any, x: any, y: any, width: any): void;
	drawActorNickname(actor: any, x: any, y: any, width: any): void;
	drawActorLevel(actor: any, x: any, y: any): void;
	drawActorIcons(actor: any, x: any, y: any, width: any): void;
	drawCurrentAndMax(
		current: any,
		max: any,
		x: any,
		y: any,
		width: any,
		color1: any,
		color2: any
	): void;
	drawActorHp(actor: any, x: any, y: any, width: any): void;
	drawActorMp(actor: any, x: any, y: any, width: any): void;
	drawActorTp(actor: any, x: any, y: any, width: any): void;
	drawActorSimpleStatus(actor: any, x: any, y: any, width: any): void;
	drawItemName(item: any, x: any, y: any, width: any): void;
	drawCurrencyValue(value: any, unit: any, x: any, y: any, width: any): void;
	paramchangeTextColor(change: any): any;
	setBackgroundType(type: any): void;
	opacity: number;
	showBackgroundDimmer(): void;
	hideBackgroundDimmer(): void;
	updateBackgroundDimmer(): void;
	refreshDimmerBitmap(): void;
	dimColor1(): string;
	dimColor2(): string;
	canvasToLocalX(x: any): any;
	canvasToLocalY(y: any): any;
	reserveFaceImages(): void;
}
declare class Window_Selectable extends Window_Base {
	constructor(...args: any[]);
	initialize(x: any, y: any, width: any, height: any): void;
	_iconWidth: number;
	_iconHeight: number;
	_faceWidth: number;
	_faceHeight: number;
	_index: any;
	_cursorFixed: any;
	_cursorAll: any;
	_stayCount: number;
	_helpWindow: any;
	_handlers: {};
	_touching: boolean;
	_scrollX: number;
	_scrollY: any;
	index(): any;
	cursorFixed(): any;
	setCursorFixed(cursorFixed: any): void;
	cursorAll(): any;
	setCursorAll(cursorAll: any): void;
	maxCols(): number;
	maxItems(): number;
	spacing(): number;
	itemWidth(): number;
	itemHeight(): any;
	maxRows(): number;
	activate(): void;
	deactivate(): void;
	select(index: any): void;
	deselect(): void;
	reselect(): void;
	row(): number;
	topRow(): number;
	maxTopRow(): number;
	setTopRow(row: any): void;
	resetScroll(): void;
	maxPageRows(): number;
	maxPageItems(): number;
	isHorizontal(): boolean;
	bottomRow(): number;
	setBottomRow(row: any): void;
	topIndex(): number;
	itemRect(index: any): any;
	itemRectForText(index: any): any;
	setHelpWindow(helpWindow: any): void;
	showHelpWindow(): void;
	hideHelpWindow(): void;
	setHandler(symbol: any, method: any): void;
	isHandled(symbol: any): boolean;
	callHandler(symbol: any): void;
	isOpenAndActive(): any;
	isCursorMovable(): boolean;
	cursorDown(wrap: any): void;
	cursorUp(wrap: any): void;
	cursorRight(wrap: any): void;
	cursorLeft(wrap: any): void;
	cursorPagedown(): void;
	cursorPageup(): void;
	scrollDown(): void;
	scrollUp(): void;
	update(): void;
	updateArrows(): void;
	downArrowVisible: boolean;
	upArrowVisible: boolean;
	processCursorMove(): void;
	processHandling(): void;
	processWheel(): void;
	processTouch(): void;
	isTouchedInsideFrame(): boolean;
	onTouch(triggered: any): void;
	hitTest(x: any, y: any): number;
	isContentsArea(x: any, y: any): boolean;
	isTouchOkEnabled(): boolean;
	isOkEnabled(): boolean;
	isCancelEnabled(): boolean;
	isOkTriggered(): any;
	isCancelTriggered(): any;
	processOk(): void;
	playOkSound(): void;
	playBuzzerSound(): void;
	callOkHandler(): void;
	processCancel(): void;
	callCancelHandler(): void;
	processPageup(): void;
	processPagedown(): void;
	updateInputData(): void;
	updateCursor(): void;
	isCursorVisible(): boolean;
	ensureCursorVisible(): void;
	callUpdateHelp(): void;
	updateHelp(): void;
	setHelpWindowItem(item: any): void;
	isCurrentItemEnabled(): boolean;
	drawAllItems(): void;
	drawItem(index: any): void;
	clearItem(index: any): void;
	redrawItem(index: any): void;
	redrawCurrentItem(): void;
	refresh(): void;
}
declare class Window_Command extends Window_Selectable {
	constructor(...args: any[]);
	initialize(x: any, y: any): void;
	windowWidth(): number;
	windowHeight(): any;
	numVisibleRows(): number;
	maxItems(): number;
	clearCommandList(): void;
	_list: any[];
	makeCommandList(): void;
	addCommand(name: any, symbol: any, enabled?: any, ext?: any): void;
	commandName(index: any): any;
	commandSymbol(index: any): any;
	isCommandEnabled(index: any): any;
	currentData(): any;
	isCurrentItemEnabled(): any;
	currentSymbol(): any;
	currentExt(): any;
	findSymbol(symbol: any): number;
	selectSymbol(symbol: any): void;
	findExt(ext: any): number;
	selectExt(ext: any): void;
	drawItem(index: any): void;
	itemTextAlign(): string;
	isOkEnabled(): boolean;
	callOkHandler(): void;
	refresh(): void;
}
declare function Window_HorzCommand(...args: any[]): void;
declare class Window_HorzCommand {
	constructor(...args: any[]);
	initialize(x: any, y: any): void;
	numVisibleRows(): number;
	maxCols(): number;
	itemTextAlign(): string;
}
declare function Window_Help(...args: any[]): void;
declare class Window_Help {
	constructor(...args: any[]);
	initialize(numLines: any): void;
	_text: any;
	setText(text: any): void;
	clear(): void;
	setItem(item: any): void;
	refresh(): void;
}
declare function Window_Gold(...args: any[]): void;
declare class Window_Gold {
	constructor(...args: any[]);
	initialize(x: any, y: any): void;
	windowWidth(): number;
	windowHeight(): any;
	refresh(): void;
	value(): any;
	currencyUnit(): any;
	open(): void;
}
declare function Window_MenuCommand(...args: any[]): void;
declare class Window_MenuCommand {
	constructor(...args: any[]);
	initialize(x: any, y: any): void;
	windowWidth(): number;
	numVisibleRows(): any;
	makeCommandList(): void;
	addMainCommands(): void;
	addFormationCommand(): void;
	addOriginalCommands(): void;
	addOptionsCommand(): void;
	addSaveCommand(): void;
	addGameEndCommand(): void;
	needsCommand(name: any): any;
	areMainCommandsEnabled(): any;
	isFormationEnabled(): any;
	isOptionsEnabled(): boolean;
	isSaveEnabled(): any;
	isGameEndEnabled(): boolean;
	processOk(): void;
	selectLast(): void;
}
declare namespace Window_MenuCommand {
	let _lastCommandSymbol: any;
	function initCommandPosition(): void;
}
declare function Window_MenuStatus(...args: any[]): void;
declare class Window_MenuStatus {
	constructor(...args: any[]);
	initialize(x: any, y: any): void;
	_formationMode: any;
	_pendingIndex: any;
	windowWidth(): number;
	windowHeight(): any;
	maxItems(): any;
	itemHeight(): number;
	numVisibleRows(): number;
	loadImages(): void;
	drawItem(index: any): void;
	drawItemBackground(index: any): void;
	drawItemImage(index: any): void;
	drawItemStatus(index: any): void;
	processOk(): void;
	isCurrentItemEnabled(): any;
	selectLast(): void;
	formationMode(): any;
	setFormationMode(formationMode: any): void;
	pendingIndex(): any;
	setPendingIndex(index: any): void;
}
declare function Window_MenuActor(...args: any[]): void;
declare class Window_MenuActor {
	constructor(...args: any[]);
	initialize(): void;
	processOk(): void;
	selectLast(): void;
	selectForItem(item: any): void;
}
declare function Window_ItemCategory(...args: any[]): void;
declare class Window_ItemCategory {
	constructor(...args: any[]);
	initialize(): void;
	windowWidth(): any;
	maxCols(): number;
	update(): void;
	makeCommandList(): void;
	setItemWindow(itemWindow: any): void;
	_itemWindow: any;
}
declare function Window_ItemList(...args: any[]): void;
declare class Window_ItemList {
	constructor(...args: any[]);
	initialize(x: any, y: any, width: any, height: any): void;
	_category: any;
	_data: any;
	setCategory(category: any): void;
	maxCols(): number;
	spacing(): number;
	maxItems(): any;
	item(): any;
	isCurrentItemEnabled(): any;
	includes(item: any): any;
	needsNumber(): boolean;
	isEnabled(item: any): any;
	makeItemList(): void;
	selectLast(): void;
	drawItem(index: any): void;
	numberWidth(): any;
	drawItemNumber(item: any, x: any, y: any, width: any): void;
	updateHelp(): void;
	refresh(): void;
}
declare function Window_SkillType(...args: any[]): void;
declare class Window_SkillType {
	constructor(...args: any[]);
	initialize(x: any, y: any): void;
	_actor: any;
	windowWidth(): number;
	setActor(actor: any): void;
	numVisibleRows(): number;
	makeCommandList(): void;
	update(): void;
	setSkillWindow(skillWindow: any): void;
	_skillWindow: any;
	selectLast(): void;
}
declare function Window_SkillStatus(...args: any[]): void;
declare class Window_SkillStatus {
	constructor(...args: any[]);
	initialize(x: any, y: any, width: any, height: any): void;
	_actor: any;
	setActor(actor: any): void;
	refresh(): void;
}
declare function Window_SkillList(...args: any[]): void;
declare class Window_SkillList {
	constructor(...args: any[]);
	initialize(x: any, y: any, width: any, height: any): void;
	_actor: any;
	_stypeId: any;
	_data: any;
	setActor(actor: any): void;
	setStypeId(stypeId: any): void;
	maxCols(): number;
	spacing(): number;
	maxItems(): any;
	item(): any;
	isCurrentItemEnabled(): any;
	includes(item: any): boolean;
	isEnabled(item: any): any;
	makeItemList(): void;
	selectLast(): void;
	drawItem(index: any): void;
	costWidth(): any;
	drawSkillCost(skill: any, x: any, y: any, width: any): void;
	updateHelp(): void;
	refresh(): void;
}
declare function Window_EquipStatus(...args: any[]): void;
declare class Window_EquipStatus {
	constructor(...args: any[]);
	initialize(x: any, y: any): void;
	_actor: any;
	_tempActor: any;
	windowWidth(): number;
	windowHeight(): any;
	numVisibleRows(): number;
	setActor(actor: any): void;
	refresh(): void;
	setTempActor(tempActor: any): void;
	drawItem(x: any, y: any, paramId: any): void;
	drawParamName(x: any, y: any, paramId: any): void;
	drawCurrentParam(x: any, y: any, paramId: any): void;
	drawRightArrow(x: any, y: any): void;
	drawNewParam(x: any, y: any, paramId: any): void;
}
declare function Window_EquipCommand(...args: any[]): void;
declare class Window_EquipCommand {
	constructor(...args: any[]);
	initialize(x: any, y: any, width: any): void;
	_windowWidth: any;
	windowWidth(): any;
	maxCols(): number;
	makeCommandList(): void;
}
declare function Window_EquipSlot(...args: any[]): void;
declare class Window_EquipSlot {
	constructor(...args: any[]);
	initialize(x: any, y: any, width: any, height: any): void;
	_actor: any;
	setActor(actor: any): void;
	update(): void;
	maxItems(): any;
	item(): any;
	drawItem(index: any): void;
	slotName(index: any): any;
	isEnabled(index: any): any;
	isCurrentItemEnabled(): any;
	setStatusWindow(statusWindow: any): void;
	_statusWindow: any;
	setItemWindow(itemWindow: any): void;
	_itemWindow: any;
	updateHelp(): void;
}
declare function Window_EquipItem(...args: any[]): void;
declare class Window_EquipItem {
	constructor(...args: any[]);
	initialize(x: any, y: any, width: any, height: any): void;
	_actor: any;
	_slotId: any;
	setActor(actor: any): void;
	setSlotId(slotId: any): void;
	includes(item: any): any;
	isEnabled(item: any): boolean;
	selectLast(): void;
	setStatusWindow(statusWindow: any): void;
	_statusWindow: any;
	updateHelp(): void;
	playOkSound(): void;
}
declare function Window_Status(...args: any[]): void;
declare class Window_Status {
	constructor(...args: any[]);
	initialize(): void;
	_actor: any;
	setActor(actor: any): void;
	refresh(): void;
	drawBlock1(y: any): void;
	drawBlock2(y: any): void;
	drawBlock3(y: any): void;
	drawBlock4(y: any): void;
	drawHorzLine(y: any): void;
	lineColor(): any;
	drawBasicInfo(x: any, y: any): void;
	drawParameters(x: any, y: any): void;
	drawExpInfo(x: any, y: any): void;
	drawEquipments(x: any, y: any): void;
	drawProfile(x: any, y: any): void;
	maxEquipmentLines(): number;
}
declare function Window_Options(...args: any[]): void;
declare class Window_Options {
	constructor(...args: any[]);
	initialize(): void;
	windowWidth(): number;
	windowHeight(): any;
	updatePlacement(): void;
	x: number;
	y: number;
	makeCommandList(): void;
	addGeneralOptions(): void;
	addVolumeOptions(): void;
	drawItem(index: any): void;
	statusWidth(): number;
	statusText(index: any): string;
	isVolumeSymbol(symbol: any): any;
	booleanStatusText(value: any): 'ON' | 'OFF';
	volumeStatusText(value: any): string;
	processOk(): void;
	cursorRight(wrap: any): void;
	cursorLeft(wrap: any): void;
	volumeOffset(): number;
	changeValue(symbol: any, value: any): void;
	getConfigValue(symbol: any): any;
	setConfigValue(symbol: any, volume: any): void;
}
declare function Window_SavefileList(...args: any[]): void;
declare class Window_SavefileList {
	constructor(...args: any[]);
	initialize(x: any, y: any, width: any, height: any): void;
	_mode: any;
	setMode(mode: any): void;
	maxItems(): any;
	maxVisibleItems(): number;
	itemHeight(): number;
	drawItem(index: any): void;
	drawFileId(id: any, x: any, y: any): void;
	drawContents(info: any, rect: any, valid: any): void;
	drawGameTitle(info: any, x: any, y: any, width: any): void;
	drawPartyCharacters(info: any, x: any, y: any): void;
	drawPlaytime(info: any, x: any, y: any, width: any): void;
	playOkSound(): void;
}
declare function Window_ShopCommand(...args: any[]): void;
declare class Window_ShopCommand {
	constructor(...args: any[]);
	initialize(width: any, purchaseOnly: any): void;
	_windowWidth: any;
	_purchaseOnly: any;
	windowWidth(): any;
	maxCols(): number;
	makeCommandList(): void;
}
declare function Window_ShopBuy(...args: any[]): void;
declare class Window_ShopBuy {
	constructor(...args: any[]);
	initialize(x: any, y: any, height: any, shopGoods: any): void;
	_shopGoods: any;
	_money: any;
	windowWidth(): number;
	maxItems(): number;
	item(): any;
	setMoney(money: any): void;
	isCurrentItemEnabled(): boolean;
	price(item: any): any;
	isEnabled(item: any): boolean;
	refresh(): void;
	makeItemList(): void;
	_data: any[];
	_price: any[];
	drawItem(index: any): void;
	setStatusWindow(statusWindow: any): void;
	_statusWindow: any;
	updateHelp(): void;
}
declare function Window_ShopSell(...args: any[]): void;
declare class Window_ShopSell {
	constructor(...args: any[]);
	initialize(x: any, y: any, width: any, height: any): void;
	isEnabled(item: any): boolean;
}
declare function Window_ShopNumber(...args: any[]): void;
declare class Window_ShopNumber {
	constructor(...args: any[]);
	initialize(x: any, y: any, height: any): void;
	_item: any;
	_max: number;
	_price: any;
	_number: any;
	_currencyUnit: any;
	windowWidth(): number;
	number(): any;
	setup(item: any, max: any, price: any): void;
	setCurrencyUnit(currencyUnit: any): void;
	createButtons(): void;
	_buttons: any[];
	placeButtons(): void;
	updateButtonsVisiblity(): void;
	showButtons(): void;
	hideButtons(): void;
	refresh(): void;
	drawMultiplicationSign(): void;
	drawNumber(): void;
	drawTotalPrice(): void;
	itemY(): number;
	priceY(): number;
	buttonY(): number;
	cursorWidth(): number;
	cursorX(): number;
	maxDigits(): number;
	update(): void;
	isOkTriggered(): any;
	playOkSound(): void;
	processNumberChange(): void;
	changeNumber(amount: any): void;
	updateCursor(): void;
	onButtonUp(): void;
	onButtonUp2(): void;
	onButtonDown(): void;
	onButtonDown2(): void;
	onButtonOk(): void;
}
declare function Window_ShopStatus(...args: any[]): void;
declare class Window_ShopStatus {
	constructor(...args: any[]);
	initialize(x: any, y: any, width: any, height: any): void;
	_item: any;
	_pageIndex: any;
	refresh(): void;
	setItem(item: any): void;
	isEquipItem(): any;
	drawPossession(x: any, y: any): void;
	drawEquipInfo(x: any, y: any): void;
	statusMembers(): any;
	pageSize(): number;
	maxPages(): number;
	drawActorEquipInfo(x: any, y: any, actor: any): void;
	drawActorParamChange(x: any, y: any, actor: any, item1: any): void;
	paramId(): 2 | 3;
	currentEquippedItem(actor: any, etypeId: any): any;
	update(): void;
	updatePage(): void;
	isPageChangeEnabled(): boolean;
	isPageChangeRequested(): boolean;
	isTouchedInsideFrame(): boolean;
	changePage(): void;
}
declare function Window_NameEdit(...args: any[]): void;
declare class Window_NameEdit {
	constructor(...args: any[]);
	initialize(actor: any, maxLength: any): void;
	_actor: any;
	_name: any;
	_index: any;
	_maxLength: any;
	_defaultName: any;
	windowWidth(): number;
	windowHeight(): any;
	name(): any;
	restoreDefault(): boolean;
	add(ch: any): boolean;
	back(): boolean;
	faceWidth(): number;
	charWidth(): any;
	left(): number;
	itemRect(index: any): {
		x: number;
		y: number;
		width: any;
		height: any;
	};
	underlineRect(index: any): {
		x: number;
		y: number;
		width: any;
		height: any;
	};
	underlineColor(): any;
	drawUnderline(index: any): void;
	drawChar(index: any): void;
	refresh(): void;
}
declare function Window_NameInput(...args: any[]): void;
declare class Window_NameInput {
	constructor(...args: any[]);
	initialize(editWindow: any): void;
	_editWindow: any;
	_page: any;
	_index: any;
	windowHeight(): any;
	table(): string[][];
	maxCols(): number;
	maxItems(): number;
	character(): string;
	isPageChange(): boolean;
	isOk(): boolean;
	itemRect(index: any): {
		x: number;
		y: number;
		width: number;
		height: any;
	};
	refresh(): void;
	updateCursor(): void;
	isCursorMovable(): any;
	cursorDown(wrap: any): void;
	cursorUp(wrap: any): void;
	cursorRight(wrap: any): void;
	cursorLeft(wrap: any): void;
	cursorPagedown(): void;
	cursorPageup(): void;
	processCursorMove(): void;
	processHandling(): void;
	isCancelEnabled(): boolean;
	processCancel(): void;
	processJump(): void;
	processBack(): void;
	processOk(): void;
	onNameAdd(): void;
	onNameOk(): void;
}
declare namespace Window_NameInput {
	let LATIN1: string[];
	let LATIN2: string[];
	let RUSSIA: string[];
	let JAPAN1: string[];
	let JAPAN2: string[];
	let JAPAN3: string[];
}
declare function Window_ChoiceList(...args: any[]): void;
declare class Window_ChoiceList {
	constructor(...args: any[]);
	initialize(messageWindow: any): void;
	_messageWindow: any;
	openness: number;
	_background: any;
	start(): void;
	selectDefault(): void;
	updatePlacement(): void;
	width: number;
	height: any;
	x: number;
	y: any;
	updateBackground(): void;
	windowWidth(): number;
	numVisibleRows(): any;
	maxChoiceWidth(): number;
	textWidthEx(text: any): any;
	contentsHeight(): number;
	makeCommandList(): void;
	drawItem(index: any): void;
	isCancelEnabled(): boolean;
	isOkTriggered(): any;
	callOkHandler(): void;
	callCancelHandler(): void;
}
declare function Window_NumberInput(...args: any[]): void;
declare class Window_NumberInput {
	constructor(...args: any[]);
	initialize(messageWindow: any): void;
	_messageWindow: any;
	_number: any;
	_maxDigits: any;
	openness: number;
	start(): void;
	updatePlacement(): void;
	width: number;
	height: any;
	x: number;
	y: any;
	windowWidth(): number;
	windowHeight(): any;
	maxCols(): any;
	maxItems(): any;
	spacing(): number;
	itemWidth(): number;
	createButtons(): void;
	_buttons: any[];
	placeButtons(): void;
	updateButtonsVisiblity(): void;
	showButtons(): void;
	hideButtons(): void;
	buttonY(): any;
	update(): void;
	processDigitChange(): void;
	changeDigit(up: any): void;
	isTouchOkEnabled(): boolean;
	isOkEnabled(): boolean;
	isCancelEnabled(): boolean;
	isOkTriggered(): any;
	processOk(): void;
	drawItem(index: any): void;
	onButtonUp(): void;
	onButtonDown(): void;
	onButtonOk(): void;
}
declare function Window_EventItem(...args: any[]): void;
declare class Window_EventItem {
	constructor(...args: any[]);
	initialize(messageWindow: any): void;
	_messageWindow: any;
	openness: number;
	windowHeight(): any;
	numVisibleRows(): number;
	start(): void;
	updatePlacement(): void;
	y: number;
	includes(item: any): boolean;
	isEnabled(item: any): boolean;
	onOk(): void;
	onCancel(): void;
}
declare function Window_Message(...args: any[]): void;
declare class Window_Message {
	constructor(...args: any[]);
	initialize(): void;
	openness: number;
	initMembers(): void;
	_imageReservationId: any;
	_background: any;
	_positionType: any;
	_waitCount: any;
	_faceBitmap: any;
	_textState: {};
	subWindows(): (
		| Window_Gold
		| Window_ChoiceList
		| Window_NumberInput
		| Window_EventItem
	)[];
	createSubWindows(): void;
	_goldWindow: Window_Gold;
	_choiceWindow: Window_ChoiceList;
	_numberWindow: Window_NumberInput;
	_itemWindow: Window_EventItem;
	windowWidth(): any;
	windowHeight(): any;
	clearFlags(): void;
	_showFast: boolean;
	_lineShowFast: boolean;
	_pauseSkip: boolean;
	numVisibleRows(): number;
	update(): void;
	checkToNotClose(): void;
	canStart(): boolean;
	startMessage(): void;
	updatePlacement(): void;
	y: number;
	updateBackground(): void;
	terminateMessage(): void;
	updateWait(): boolean;
	updateLoading(): boolean;
	updateInput(): boolean;
	pause: boolean;
	isAnySubWindowActive(): any;
	updateMessage(): boolean;
	onEndOfText(): void;
	startInput(): boolean;
	isTriggered(): any;
	doesContinue(): boolean;
	areSettingsChanged(): boolean;
	updateShowFast(): void;
	newPage(textState: any): void;
	loadMessageFace(): void;
	drawMessageFace(): void;
	newLineX(): 0 | 168;
	processNewLine(textState: any): void;
	processNewPage(textState: any): void;
	isEndOfText(textState: any): boolean;
	needsNewPage(textState: any): boolean;
	processEscapeCharacter(code: any, textState: any): void;
	startWait(count: any): void;
	startPause(): void;
}
declare function Window_ScrollText(...args: any[]): void;
declare class Window_ScrollText {
	constructor(...args: any[]);
	initialize(): void;
	opacity: number;
	_text: any;
	_allTextHeight: any;
	update(): void;
	startMessage(): void;
	refresh(): void;
	contentsHeight(): number;
	updateMessage(): void;
	scrollSpeed(): number;
	isFastForward(): any;
	fastForwardRate(): number;
	terminateMessage(): void;
}
declare function Window_MapName(...args: any[]): void;
declare class Window_MapName {
	constructor(...args: any[]);
	initialize(): void;
	opacity: number;
	contentsOpacity: number;
	_showCount: number;
	windowWidth(): number;
	windowHeight(): any;
	update(): void;
	updateFadeIn(): void;
	updateFadeOut(): void;
	open(): void;
	close(): void;
	refresh(): void;
	drawBackground(x: any, y: any, width: any, height: any): void;
}
declare function Window_BattleLog(...args: any[]): void;
declare class Window_BattleLog {
	constructor(...args: any[]);
	initialize(): void;
	opacity: number;
	_lines: any[];
	_methods: any[];
	_waitCount: number;
	_waitMode: any;
	_baseLineStack: any[];
	_spriteset: any;
	setSpriteset(spriteset: any): void;
	windowWidth(): any;
	windowHeight(): any;
	maxLines(): number;
	createBackBitmap(): void;
	_backBitmap: any;
	createBackSprite(): void;
	_backSprite: any;
	numLines(): number;
	messageSpeed(): number;
	isBusy(): any;
	update(): void;
	updateWait(): boolean;
	updateWaitCount(): boolean;
	updateWaitMode(): boolean;
	setWaitMode(waitMode: any): void;
	callNextMethod(): void;
	isFastForward(): any;
	push(methodName: any, ...args: any[]): void;
	clear(): void;
	wait(): void;
	waitForEffect(): void;
	waitForMovement(): void;
	addText(text: any): void;
	pushBaseLine(): void;
	popBaseLine(): void;
	waitForNewLine(): void;
	popupDamage(target: any): void;
	performActionStart(subject: any, action: any): void;
	performAction(subject: any, action: any): void;
	performActionEnd(subject: any): void;
	performDamage(target: any): void;
	performMiss(target: any): void;
	performRecovery(target: any): void;
	performEvasion(target: any): void;
	performMagicEvasion(target: any): void;
	performCounter(target: any): void;
	performReflection(target: any): void;
	performSubstitute(substitute: any, target: any): void;
	performCollapse(target: any): void;
	showAnimation(subject: any, targets: any, animationId: any): void;
	showAttackAnimation(subject: any, targets: any): void;
	showActorAttackAnimation(subject: any, targets: any): void;
	showEnemyAttackAnimation(subject: any, targets: any): void;
	showNormalAnimation(targets: any, animationId: any, mirror: any): void;
	animationBaseDelay(): number;
	animationNextDelay(): number;
	refresh(): void;
	drawBackground(): void;
	backRect(): {
		x: number;
		y: any;
		width: any;
		height: number;
	};
	backColor(): string;
	backPaintOpacity(): number;
	drawLineText(index: any): void;
	startTurn(): void;
	startAction(subject: any, action: any, targets: any): void;
	endAction(subject: any): void;
	displayCurrentState(subject: any): void;
	displayRegeneration(subject: any): void;
	displayAction(subject: any, item: any): void;
	displayCounter(target: any): void;
	displayReflection(target: any): void;
	displaySubstitute(substitute: any, target: any): void;
	displayActionResults(subject: any, target: any): void;
	displayFailure(target: any): void;
	displayCritical(target: any): void;
	displayDamage(target: any): void;
	displayMiss(target: any): void;
	displayEvasion(target: any): void;
	displayHpDamage(target: any): void;
	displayMpDamage(target: any): void;
	displayTpDamage(target: any): void;
	displayAffectedStatus(target: any): void;
	displayAutoAffectedStatus(target: any): void;
	displayChangedStates(target: any): void;
	displayAddedStates(target: any): void;
	displayRemovedStates(target: any): void;
	displayChangedBuffs(target: any): void;
	displayBuffs(target: any, buffs: any, fmt: any): void;
	makeHpDamageText(target: any): any;
	makeMpDamageText(target: any): any;
	makeTpDamageText(target: any): any;
}
declare function Window_PartyCommand(...args: any[]): void;
declare class Window_PartyCommand {
	constructor(...args: any[]);
	initialize(): void;
	openness: number;
	windowWidth(): number;
	numVisibleRows(): number;
	makeCommandList(): void;
	setup(): void;
}
declare function Window_ActorCommand(...args: any[]): void;
declare class Window_ActorCommand {
	constructor(...args: any[]);
	initialize(): void;
	openness: number;
	_actor: any;
	windowWidth(): number;
	numVisibleRows(): number;
	makeCommandList(): void;
	addAttackCommand(): void;
	addSkillCommands(): void;
	addGuardCommand(): void;
	addItemCommand(): void;
	setup(actor: any): void;
	processOk(): void;
	selectLast(): void;
}
declare function Window_BattleStatus(...args: any[]): void;
declare class Window_BattleStatus {
	constructor(...args: any[]);
	initialize(): void;
	openness: number;
	windowWidth(): number;
	windowHeight(): any;
	numVisibleRows(): number;
	maxItems(): any;
	refresh(): void;
	drawItem(index: any): void;
	basicAreaRect(index: any): any;
	gaugeAreaRect(index: any): any;
	gaugeAreaWidth(): number;
	drawBasicArea(rect: any, actor: any): void;
	drawGaugeArea(rect: any, actor: any): void;
	drawGaugeAreaWithTp(rect: any, actor: any): void;
	drawGaugeAreaWithoutTp(rect: any, actor: any): void;
}
declare function Window_BattleActor(...args: any[]): void;
declare class Window_BattleActor {
	constructor(...args: any[]);
	initialize(x: any, y: any): void;
	x: any;
	y: any;
	openness: number;
	show(): void;
	hide(): void;
	select(index: any): void;
	actor(): any;
}
declare function Window_BattleEnemy(...args: any[]): void;
declare class Window_BattleEnemy {
	constructor(...args: any[]);
	initialize(x: any, y: any): void;
	_enemies: any;
	windowWidth(): number;
	windowHeight(): any;
	numVisibleRows(): number;
	maxCols(): number;
	maxItems(): any;
	enemy(): any;
	enemyIndex(): any;
	drawItem(index: any): void;
	show(): void;
	hide(): void;
	refresh(): void;
	select(index: any): void;
}
declare function Window_BattleSkill(...args: any[]): void;
declare class Window_BattleSkill {
	constructor(...args: any[]);
	initialize(x: any, y: any, width: any, height: any): void;
	show(): void;
	hide(): void;
}
declare function Window_BattleItem(...args: any[]): void;
declare class Window_BattleItem {
	constructor(...args: any[]);
	initialize(x: any, y: any, width: any, height: any): void;
	includes(item: any): any;
	show(): void;
	hide(): void;
}
declare class Window_TitleCommand extends Window_Command {
	constructor(...args: any[]);
	initialize(): void;
	openness: number;
	windowWidth(): number;
	updatePlacement(): void;
	x: number;
	y: number;
	makeCommandList(): void;
	isContinueEnabled(): any;
	processOk(): void;
	selectLast(): void;
}
declare namespace Window_TitleCommand {
	let _lastCommandSymbol_1: any;
	export { _lastCommandSymbol_1 as _lastCommandSymbol };
	export function initCommandPosition(): void;
}
declare function Window_GameEnd(...args: any[]): void;
declare class Window_GameEnd {
	constructor(...args: any[]);
	initialize(): void;
	openness: number;
	windowWidth(): number;
	updatePlacement(): void;
	x: number;
	y: number;
	makeCommandList(): void;
}
declare function Window_DebugRange(...args: any[]): void;
declare class Window_DebugRange {
	constructor(...args: any[]);
	initialize(x: any, y: any): void;
	_maxSwitches: number;
	_maxVariables: number;
	windowWidth(): number;
	windowHeight(): any;
	maxItems(): number;
	update(): void;
	mode(): 'variable' | 'switch';
	topId(): number;
	refresh(): void;
	drawItem(index: any): void;
	isCancelTriggered(): any;
	processCancel(): void;
	setEditWindow(editWindow: any): void;
	_editWindow: any;
}
declare namespace Window_DebugRange {
	let lastTopRow: number;
	let lastIndex: number;
}
declare function Window_DebugEdit(...args: any[]): void;
declare class Window_DebugEdit {
	constructor(...args: any[]);
	initialize(x: any, y: any, width: any): void;
	_mode: any;
	_topId: any;
	maxItems(): number;
	refresh(): void;
	drawItem(index: any): void;
	itemName(dataId: any): any;
	itemStatus(dataId: any): string;
	setMode(mode: any): void;
	setTopId(id: any): void;
	currentId(): any;
	update(): void;
	updateSwitch(): void;
	updateVariable(): void;
}
