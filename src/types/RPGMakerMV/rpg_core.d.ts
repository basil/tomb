/**
 * The window in the game.
 *
 * NOTE: This class is actually called just "Window," but that would clash with the default "Window" global
 *
 * @class Window
 * @constructor
 */
declare class __RPG_Window extends PIXI.Container {}
/**
 * The basic object that represents an image.
 */
declare class Bitmap {
	/**
	 * @param height The width of the bitmap
	 * @param width The height of the bitmap
	 */
	constructor(height: number, width: number);
	/**
	 * The face name of the font.
	 */
	fontFace: string;
	/**
	 * The size of the font in pixels.
	 */
	fontSize: number;
	/**
	 * Whether the font is italic.
	 */
	fontItalic: boolean;
	/**
	 * The color of the text in CSS format.
	 */
	textColor: string;
	/**
	 * The color of the outline of the text in CSS format.
	 */
	outlineColor: string;
	/**
	 * The width of the outline of the text.
	 */
	outlineWidth: number;
	/**
	 * Draws the outline text to the bitmap.
	 *
	 * @param text The text that will be drawn
	 * @param x The x coordinate for the left of the text
	 * @param y The y coordinate for the top of the text
	 * @param maxWidth The maximum allowed width of the text
	 * @param lineHeight The height of the text line
	 * @param align The alignment of the text
	 */
	drawText(
		text: string,
		x: number,
		y: number,
		maxWidth: number,
		lineHeight: number,
		align: 'left' | 'center' | 'right'
	): void;
}

declare class Sprite extends PIXI.Sprite {
	constructor(bitmap: Bitmap);
	bitmap: Bitmap;
}
declare class Input {
	static isTriggered(keyName: string): boolean;
}

/**
 * The static class that carries out graphics processing.
 */
declare class Graphics {
	/**
	 * The width of the game screen.
	 */
	static width: number;
	/**
	 * The height of the game screen.
	 */
	static height: number;
	/**
	 * The width of the window display area.
	 */
	static boxWidth: number;
	/**
	 * The height of the window display area.
	 */
	static boxHeight: number;
}
