/**
 * The Superclass of all scene within the game.
 *
 * @class Scene_Base
 * @constructor
 * @extends Stage
 */
declare function Scene_Base(...args: any[]): void;
declare class Scene_Base {
	/**
	 * The Superclass of all scene within the game.
	 *
	 * @class Scene_Base
	 * @constructor
	 * @extends Stage
	 */
	constructor(...args: any[]);
	/**
	 * Create a instance of Scene_Base.
	 *
	 * @instance
	 * @memberof Scene_Base
	 */
	initialize(): void;
	_active: boolean;
	_fadeSign: number;
	_fadeDuration: number;
	_fadeSprite: any;
	_imageReservationId: any;
	/**
	 * Attach a reservation to the reserve queue.
	 *
	 * @method attachReservation
	 * @instance
	 * @memberof Scene_Base
	 */
	attachReservation(): void;
	/**
	 * Remove the reservation from the Reserve queue.
	 *
	 * @method detachReservation
	 * @instance
	 * @memberof Scene_Base
	 */
	detachReservation(): void;
	/**
	 * Create the components and add them to the rendering process.
	 *
	 * @method create
	 * @instance
	 * @memberof Scene_Base
	 */
	create(): void;
	/**
	 * Returns whether the scene is active or not.
	 *
	 * @method isActive
	 * @instance
	 * @memberof Scene_Base
	 * @return {Boolean} return true if the scene is active
	 */
	isActive(): boolean;
	/**
	 * Return whether the scene is ready to start or not.
	 *
	 * @method isReady
	 * @instance
	 * @memberof Scene_Base
	 * @return {Boolean} Return true if the scene is ready to start
	 */
	isReady(): boolean;
	/**
	 * Start the scene processing.
	 *
	 * @method start
	 * @instance
	 * @memberof Scene_Base
	 */
	start(): void;
	/**
	 * Update the scene processing each new frame.
	 *
	 * @method update
	 * @instance
	 * @memberof Scene_Base
	 */
	update(): void;
	/**
	 * Stop the scene processing.
	 *
	 * @method stop
	 * @instance
	 * @memberof Scene_Base
	 */
	stop(): void;
	/**
	 * Return whether the scene is busy or not.
	 *
	 * @method isBusy
	 * @instance
	 * @memberof Scene_Base
	 * @return {Boolean} Return true if the scene is currently busy
	 */
	isBusy(): boolean;
	/**
	 * Terminate the scene before switching to a another scene.
	 *
	 * @method terminate
	 * @instance
	 * @memberof Scene_Base
	 */
	terminate(): void;
	/**
	 * Create the layer for the windows children
	 * and add it to the rendering process.
	 *
	 * @method createWindowLayer
	 * @instance
	 * @memberof Scene_Base
	 */
	createWindowLayer(): void;
	_windowLayer: any;
	/**
	 * Add the children window to the windowLayer processing.
	 *
	 * @method addWindow
	 * @instance
	 * @memberof Scene_Base
	 */
	addWindow(window: Window_Base): void;
	/**
	 * Request a fadeIn screen process.
	 *
	 * @method startFadeIn
	 * @param {Number} [duration=30] The time the process will take for fadeIn the screen
	 * @param {Boolean} [white=false] If true the fadein will be process with a white color else it's will be black
	 *
	 * @instance
	 * @memberof Scene_Base
	 */
	startFadeIn(duration?: number, white?: boolean): void;
	/**
	 * Request a fadeOut screen process.
	 *
	 * @method startFadeOut
	 * @param {Number} [duration=30] The time the process will take for fadeOut the screen
	 * @param {Boolean} [white=false] If true the fadeOut will be process with a white color else it's will be black
	 *
	 * @instance
	 * @memberof Scene_Base
	 */
	startFadeOut(duration?: number, white?: boolean): void;
	/**
	 * Create a Screen sprite for the fadein and fadeOut purpose and
	 * add it to the rendering process.
	 *
	 * @method createFadeSprite
	 * @instance
	 * @memberof Scene_Base
	 */
	createFadeSprite(white: any): void;
	/**
	 * Update the screen fade processing.
	 *
	 * @method updateFade
	 * @instance
	 * @memberof Scene_Base
	 */
	updateFade(): void;
	/**
	 * Update the children of the scene EACH frame.
	 *
	 * @method updateChildren
	 * @instance
	 * @memberof Scene_Base
	 */
	updateChildren(): void;
	/**
	 * Pop the scene from the stack array and switch to the
	 * previous scene.
	 *
	 * @method popScene
	 * @instance
	 * @memberof Scene_Base
	 */
	popScene(): void;
	/**
	 * Check whether the game should be triggering a gameover.
	 *
	 * @method checkGameover
	 * @instance
	 * @memberof Scene_Base
	 */
	checkGameover(): void;
	/**
	 * Slowly fade out all the visual and audio of the scene.
	 *
	 * @method fadeOutAll
	 * @instance
	 * @memberof Scene_Base
	 */
	fadeOutAll(): void;
	/**
	 * Return the screen fade speed value.
	 *
	 * @method fadeSpeed
	 * @instance
	 * @memberof Scene_Base
	 * @return {Number} Return the fade speed
	 */
	fadeSpeed(): number;
	/**
	 * Return a slow screen fade speed value.
	 *
	 * @method slowFadeSpeed
	 * @instance
	 * @memberof Scene_Base
	 * @return {Number} Return the fade speed
	 */
	slowFadeSpeed(): number;
}
declare function Scene_Boot(...args: any[]): void;
declare class Scene_Boot {
	constructor(...args: any[]);
	initialize(): void;
	_startDate: number;
	create(): void;
	loadSystemWindowImage(): void;
	isReady(): boolean;
	isGameFontLoaded(): boolean;
	start(): void;
	updateDocumentTitle(): void;
	checkPlayerLocation(): void;
}
declare namespace Scene_Boot {
	function loadSystemImages(): void;
}
declare function Scene_Title(...args: any[]): void;
declare class Scene_Title {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	start(): void;
	update(): void;
	isBusy(): any;
	terminate(): void;
	createBackground(): void;
	_backSprite1: any;
	_backSprite2: any;
	createForeground(): void;
	_gameTitleSprite: any;
	drawGameTitle(): void;
	centerSprite(sprite: any): void;
	createCommandWindow(): void;
	_commandWindow: Window_TitleCommand;
	commandNewGame(): void;
	commandContinue(): void;
	commandOptions(): void;
	playTitleMusic(): void;
}
declare function Scene_Map(...args: any[]): void;
declare class Scene_Map {
	constructor(...args: any[]);
	initialize(): void;
	_waitCount: number;
	_encounterEffectDuration: number;
	_mapLoaded: boolean;
	_touchCount: number;
	create(): void;
	_transfer: any;
	isReady(): any;
	onMapLoaded(): void;
	start(): void;
	menuCalling: boolean;
	update(): void;
	updateMainMultiply(): void;
	updateMain(): void;
	isFastForward(): any;
	stop(): void;
	isBusy(): any;
	terminate(): void;
	needsFadeIn(): any;
	needsSlowFadeOut(): any;
	updateWaitCount(): boolean;
	updateDestination(): void;
	isMapTouchOk(): any;
	processMapTouch(): void;
	isSceneChangeOk(): boolean;
	updateScene(): void;
	createDisplayObjects(): void;
	createSpriteset(): void;
	_spriteset: Spriteset_Map;
	createAllWindows(): void;
	createMapNameWindow(): void;
	_mapNameWindow: Window_MapName;
	createMessageWindow(): void;
	_messageWindow: Window_Message;
	createScrollTextWindow(): void;
	_scrollTextWindow: Window_ScrollText;
	updateTransferPlayer(): void;
	updateEncounter(): void;
	updateCallMenu(): void;
	isMenuEnabled(): boolean;
	isMenuCalled(): any;
	callMenu(): void;
	updateCallDebug(): void;
	isDebugCalled(): any;
	fadeInForTransfer(): void;
	fadeOutForTransfer(): void;
	launchBattle(): void;
	stopAudioOnBattleStart(): void;
	startEncounterEffect(): void;
	updateEncounterEffect(): void;
	snapForBattleBackground(): void;
	startFlashForEncounter(duration: any): void;
	encounterEffectSpeed(): number;
}
declare class Scene_MenuBase extends Scene_Base {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	actor(): any;
	updateActor(): void;
	_actor: any;
	createBackground(): void;
	_backgroundSprite: any;
	setBackgroundOpacity(opacity: any): void;
	createHelpWindow(): void;
	_helpWindow: Window_Help;
	nextActor(): void;
	previousActor(): void;
	onActorChange(): void;
}
declare function Scene_Menu(...args: any[]): void;
declare class Scene_Menu {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	start(): void;
	createCommandWindow(): void;
	_commandWindow: Window_MenuCommand;
	createGoldWindow(): void;
	_goldWindow: Window_Gold;
	createStatusWindow(): void;
	_statusWindow: Window_MenuStatus;
	commandItem(): void;
	commandPersonal(): void;
	commandFormation(): void;
	commandOptions(): void;
	commandSave(): void;
	commandGameEnd(): void;
	onPersonalOk(): void;
	onPersonalCancel(): void;
	onFormationOk(): void;
	onFormationCancel(): void;
}
declare function Scene_ItemBase(...args: any[]): void;
declare class Scene_ItemBase {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	createActorWindow(): void;
	_actorWindow: Window_MenuActor;
	item(): any;
	user(): any;
	isCursorLeft(): boolean;
	showSubWindow(window: any): void;
	hideSubWindow(window: any): void;
	onActorOk(): void;
	onActorCancel(): void;
	determineItem(): void;
	useItem(): void;
	activateItemWindow(): void;
	itemTargetActors(): any;
	canUse(): any;
	isItemEffectsValid(): any;
	applyItem(): void;
	checkCommonEvent(): void;
}
declare function Scene_Item(...args: any[]): void;
declare class Scene_Item {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	createCategoryWindow(): void;
	_categoryWindow: Window_ItemCategory;
	createItemWindow(): void;
	_itemWindow: Window_ItemList;
	user(): any;
	onCategoryOk(): void;
	onItemOk(): void;
	onItemCancel(): void;
	playSeForItem(): void;
	useItem(): void;
}
declare function Scene_Skill(...args: any[]): void;
declare class Scene_Skill {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	start(): void;
	createSkillTypeWindow(): void;
	_skillTypeWindow: Window_SkillType;
	createStatusWindow(): void;
	_statusWindow: Window_SkillStatus;
	createItemWindow(): void;
	_itemWindow: Window_SkillList;
	refreshActor(): void;
	user(): any;
	commandSkill(): void;
	onItemOk(): void;
	onItemCancel(): void;
	playSeForItem(): void;
	useItem(): void;
	onActorChange(): void;
}
declare function Scene_Equip(...args: any[]): void;
declare class Scene_Equip {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	createStatusWindow(): void;
	_statusWindow: Window_EquipStatus;
	createCommandWindow(): void;
	_commandWindow: Window_EquipCommand;
	createSlotWindow(): void;
	_slotWindow: Window_EquipSlot;
	createItemWindow(): void;
	_itemWindow: Window_EquipItem;
	refreshActor(): void;
	commandEquip(): void;
	commandOptimize(): void;
	commandClear(): void;
	onSlotOk(): void;
	onSlotCancel(): void;
	onItemOk(): void;
	onItemCancel(): void;
	onActorChange(): void;
}
declare function Scene_Status(...args: any[]): void;
declare class Scene_Status {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	_statusWindow: Window_Status;
	start(): void;
	refreshActor(): void;
	onActorChange(): void;
}
declare function Scene_Options(...args: any[]): void;
declare class Scene_Options {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	terminate(): void;
	createOptionsWindow(): void;
	_optionsWindow: Window_Options;
}
declare function Scene_File(...args: any[]): void;
declare class Scene_File {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	start(): void;
	savefileId(): any;
	createHelpWindow(): void;
	_helpWindow: Window_Help;
	createListWindow(): void;
	_listWindow: Window_SavefileList;
	mode(): any;
	activateListWindow(): void;
	helpWindowText(): string;
	firstSavefileIndex(): number;
	onSavefileOk(): void;
}
declare function Scene_Save(...args: any[]): void;
declare class Scene_Save {
	constructor(...args: any[]);
	initialize(): void;
	mode(): string;
	helpWindowText(): any;
	firstSavefileIndex(): number;
	onSavefileOk(): void;
	onSaveSuccess(): void;
	onSaveFailure(): void;
}
declare function Scene_Load(...args: any[]): void;
declare class Scene_Load {
	constructor(...args: any[]);
	initialize(): void;
	_loadSuccess: boolean;
	terminate(): void;
	mode(): string;
	helpWindowText(): any;
	firstSavefileIndex(): number;
	onSavefileOk(): void;
	onLoadSuccess(): void;
	onLoadFailure(): void;
	reloadMapIfUpdated(): void;
}
declare function Scene_GameEnd(...args: any[]): void;
declare class Scene_GameEnd {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	stop(): void;
	createBackground(): void;
	createCommandWindow(): void;
	_commandWindow: Window_GameEnd;
	commandToTitle(): void;
}
declare function Scene_Shop(...args: any[]): void;
declare class Scene_Shop {
	constructor(...args: any[]);
	initialize(): void;
	prepare(goods: any, purchaseOnly: any): void;
	_goods: any;
	_purchaseOnly: any;
	_item: any;
	create(): void;
	createGoldWindow(): void;
	_goldWindow: Window_Gold;
	createCommandWindow(): void;
	_commandWindow: Window_ShopCommand;
	createDummyWindow(): void;
	_dummyWindow: Window_Base;
	createNumberWindow(): void;
	_numberWindow: Window_ShopNumber;
	createStatusWindow(): void;
	_statusWindow: Window_ShopStatus;
	createBuyWindow(): void;
	_buyWindow: Window_ShopBuy;
	createCategoryWindow(): void;
	_categoryWindow: Window_ItemCategory;
	createSellWindow(): void;
	_sellWindow: Window_ShopSell;
	activateBuyWindow(): void;
	activateSellWindow(): void;
	commandBuy(): void;
	commandSell(): void;
	onBuyOk(): void;
	onBuyCancel(): void;
	onCategoryOk(): void;
	onCategoryCancel(): void;
	onSellOk(): void;
	onSellCancel(): void;
	onNumberOk(): void;
	onNumberCancel(): void;
	doBuy(number: any): void;
	doSell(number: any): void;
	endNumberInput(): void;
	maxBuy(): number;
	maxSell(): any;
	money(): any;
	currencyUnit(): any;
	buyingPrice(): any;
	sellingPrice(): number;
}
declare function Scene_Name(...args: any[]): void;
declare class Scene_Name {
	constructor(...args: any[]);
	initialize(): void;
	prepare(actorId: any, maxLength: any): void;
	_actorId: any;
	_maxLength: any;
	create(): void;
	_actor: any;
	start(): void;
	createEditWindow(): void;
	_editWindow: Window_NameEdit;
	createInputWindow(): void;
	_inputWindow: Window_NameInput;
	onInputOk(): void;
}
declare function Scene_Debug(...args: any[]): void;
declare class Scene_Debug {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	createRangeWindow(): void;
	_rangeWindow: Window_DebugRange;
	createEditWindow(): void;
	_editWindow: Window_DebugEdit;
	createDebugHelpWindow(): void;
	_debugHelpWindow: Window_Base;
	onRangeOk(): void;
	onEditCancel(): void;
	refreshHelpWindow(): void;
	helpText(): string;
}
declare function Scene_Battle(...args: any[]): void;
declare class Scene_Battle {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	start(): void;
	update(): void;
	updateBattleProcess(): void;
	isAnyInputWindowActive(): any;
	changeInputWindow(): void;
	stop(): void;
	terminate(): void;
	needsSlowFadeOut(): any;
	updateStatusWindow(): void;
	updateWindowPositions(): void;
	createDisplayObjects(): void;
	createSpriteset(): void;
	_spriteset: Spriteset_Battle;
	createAllWindows(): void;
	createLogWindow(): void;
	_logWindow: Window_BattleLog;
	createStatusWindow(): void;
	_statusWindow: Window_BattleStatus;
	createPartyCommandWindow(): void;
	_partyCommandWindow: Window_PartyCommand;
	createActorCommandWindow(): void;
	_actorCommandWindow: Window_ActorCommand;
	createHelpWindow(): void;
	_helpWindow: Window_Help;
	createSkillWindow(): void;
	_skillWindow: Window_BattleSkill;
	createItemWindow(): void;
	_itemWindow: Window_BattleItem;
	createActorWindow(): void;
	_actorWindow: Window_BattleActor;
	createEnemyWindow(): void;
	_enemyWindow: Window_BattleEnemy;
	createMessageWindow(): void;
	_messageWindow: Window_Message;
	createScrollTextWindow(): void;
	_scrollTextWindow: Window_ScrollText;
	refreshStatus(): void;
	startPartyCommandSelection(): void;
	commandFight(): void;
	commandEscape(): void;
	startActorCommandSelection(): void;
	commandAttack(): void;
	commandSkill(): void;
	commandGuard(): void;
	commandItem(): void;
	selectNextCommand(): void;
	selectPreviousCommand(): void;
	selectActorSelection(): void;
	onActorOk(): void;
	onActorCancel(): void;
	selectEnemySelection(): void;
	onEnemyOk(): void;
	onEnemyCancel(): void;
	onSkillOk(): void;
	onSkillCancel(): void;
	onItemOk(): void;
	onItemCancel(): void;
	onSelectAction(): void;
	endCommandSelection(): void;
}
declare function Scene_Gameover(...args: any[]): void;
declare class Scene_Gameover {
	constructor(...args: any[]);
	initialize(): void;
	create(): void;
	start(): void;
	update(): void;
	stop(): void;
	terminate(): void;
	playGameoverMusic(): void;
	createBackground(): void;
	_backSprite: any;
	isTriggered(): any;
	gotoTitle(): void;
}
