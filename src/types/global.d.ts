import type Tomb from '../tomb/classes/tomb.ts';
import type { RPGMakerPlugin } from '../types/RPGMaker.ts';
import type { CoffinLang } from './Coffin.ts';

declare global {
	// Tomb
	interface Window {
		$tomb: Tomb;
	}
	const $tomb: Tomb;

	// RPGMaker
	interface Window {
		$plugins: RPGMakerPlugin[];
	}

	// NW.js
	namespace nw {
		export const __dirname: string;
	}
	namespace NodeJS {
		export interface ProcessVersions {
			'nw-flavor': 'sdk' | 'normal';
		}
	}

	// TCoAaL
	const GAME_VERSION: string | undefined; // 2.0.12+
	const VERSION: string | undefined; // 2.0.11 and under
	const SIGNATURE: string;
	const LANG_DIR: string;
	const VALID_EXT: string[];
	class App {
		static rootPath(): string;
		static rootPath(): string;
	}
	class Lang {
		static loadLOC(filePath: string): CoffinLang;
		static loadTXT(filePath: string): CoffinLang;
		static loadCSV(filePath: string): CoffinLang;
	}
	class Utils {
		static folders(path: string): string[];
		static files(path: string): string[];
	}
	class MenuOptions {
		static orderAndIcons: Record<string, string>;
	}

	// Haven't figured this part out.
	// interface Crypto {
	// 	decrypt(buffer: Uint8Array, url: string, guard?: number): Uint8Array;
	// 	guard(): number;
	// }
}
