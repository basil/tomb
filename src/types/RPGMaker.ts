export type RPGMakerPlugin = {
	name: string;
	status: boolean;
	description: string;
	parameters: Record<string, string>;
};
