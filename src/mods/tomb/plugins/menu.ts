import fs from 'fs';
import path from 'path';
import genProject from './lib/genProject.ts';
import genMod from './lib/genMod.ts';
import { gameVersion } from '../../../tomb/util.ts';

{
	// Add some messages on the title screen
	const lines = [
		`${$tomb.mods.length} mod${$tomb.mods.length === 1 ? '' : 's'} loaded`,
		process.env.TOMB_VERSION_FORMATTED,
		`The Coffin of Andy and Leyley v${gameVersion()}`
	].reverse();

	$tomb.lib.spitroast.after('create', Scene_Title.prototype, function () {
		this._tomb_messages = [];

		lines.forEach((line, i) => {
			const text = new Sprite(new Bitmap(Graphics.width, Graphics.height));
			this._tomb_messages.push(text);
			this.addChild(text);

			const fontSize = 16;
			text.bitmap.outlineColor = 'black';
			text.bitmap.outlineWidth = 2;
			text.bitmap.fontSize = fontSize;

			const padding = 4;
			const x = 4;
			const y = Graphics.height - (fontSize + padding) * (i + 1);
			const maxWidth = Graphics.width - x * 2;

			text.bitmap.drawText(line, x, y, maxWidth, fontSize, 'left');
		});
	});

	// Add a menu option to

	const optionText = 'Tomb';
	const optionIcon = 'wrench';

	// This causes the game to load the wrench icon
	// We also have to add it later in the code.
	MenuOptions.orderAndIcons[optionText] = optionIcon;

	class Window_Tomb extends Window_Command {
		constructor() {
			super();
		}

		initialize() {
			Window_Command.prototype.initialize.call(this, 0, 0);
			this.x = (Graphics.boxWidth - this.width) / 2;
			this.y = (Graphics.boxHeight - this.height) / 2;
		}

		makeCommandList() {
			const showGenProject = !fs.existsSync('project');
			this.addCommand(
				'Generate RPG Maker MV Project',
				'genProject',
				showGenProject
			);
			this.addCommand('Bundle', 'bundle', !showGenProject);

			// this.addCommand('Reload on Mod Update', 'reloadOnModUpdate', true, 0);
		}

		windowWidth() {
			return 400;
		}
		numVisibleRows() {
			return 5;
		}

		update() {
			Window_Command.prototype.update.call(this);
			if (Input.isTriggered('cancel')) {
				SceneManager.pop();
				SoundManager.playCancel();
			}
		}

		refresh() {
			Window_Selectable.prototype.refresh.call(this);
		}

		processOk() {
			const index = this.index();
			const id = this.commandSymbol(index);
			const enabled = this.isCommandEnabled(index);

			switch (id) {
				case 'genProject':
					if (!enabled)
						return alert(
							'There\'s already a folder called "project" in the game directory.'
						);

					this._list[index].enabled = false;
					this.refresh();

					genProject()
						.then(() => {
							const bundleIndex = this._list.findIndex(
								(item) => item.symbol === 'bundle'
							);
							this._list[index].enabled = false;
							this._list[bundleIndex].enabled = true;

							nw.Shell.showItemInFolder(
								path.join(nw.__dirname, 'project', 'Game.rpgproject')
							);
						})
						.catch((e) => {
							this._list[index].enabled = true;

							console.error(e);
							alert('An error ocurred :(');
						})
						.finally(() => {
							this.refresh();
						});

					break;
				case 'bundle': {
					if (!enabled) return alert('Already running bundle.');

					this._list[index].enabled = false;
					this.refresh();

					const modId = prompt('Mod ID');

					genMod(modId)
						.then(() => {
							nw.Shell.showItemInFolder(
								path.join(nw.__dirname, 'tomb/mods', modId)
							);
						})
						.catch((e) => {
							console.error(e);
							alert('An error ocurred :(');
						})
						.finally(() => {
							this._list[index].enabled = true;
							this.refresh();
						});
				}
			}
		}
	}

	class Scene_Tomb extends Scene_MenuBase {
		constructor() {
			super();
		}

		create() {
			Scene_MenuBase.prototype.create.call(this);
			this.setBackgroundOpacity(128);
			this.addWindow(new Window_Tomb());
		}
	}

	// Insert the "Tomb" button after the "Options" button
	$tomb.lib.spitroast.after(
		'makeCommandList',
		Window_TitleCommand.prototype,
		function () {
			const fnThis = this as Window_TitleCommand;
			fnThis.addCommand(optionText, optionIcon);
			// Move the new item underneath the options button. It'll be the last one, since we just added it.
			const optionsIndex = fnThis._list.findIndex(
				(option) => option.symbol === 'options'
			);
			if (optionsIndex > -1)
				fnThis._list.splice(optionsIndex + 1, 0, fnThis._list.pop());
		}
	);

	$tomb.lib.spitroast.after(
		'createCommandWindow',
		Scene_Title.prototype,
		function () {
			this._commandWindow.setHandler(optionIcon, () => {
				SceneManager.push(Scene_Tomb);
			});
		}
	);
}
