import type { ModJSON } from '../../../../tomb/classes/mod.ts';

import fs from 'fs';
import path from 'path';
import { writeFile } from './util/fs.ts';
import diffData from './genMod/diffData.ts';
import diffLanguages from './genMod/diffLanguages.ts';
import diffImages from './genMod/diffImages.ts';
import diffAudio from './genMod/diffAudio.ts';
import diffVideos from './genMod/diffVideos.ts';
import diffPlugins from './genMod/diffPlugins.ts';
import { gameVersion } from '../../../../tomb/util.ts';

export default async function genMod(modId: string) {
	const modPath = path.join('tomb/mods/', modId);

	let json: ModJSON = {
		id: modId,
		name: modId,
		authors: ['Your Name'],
		description: 'Enter a description here!',
		version: '1.0.0',
		dependencies: {
			game: gameVersion(),
			spec: '0.1.0'
		},
		files: {
			assets: [],
			dataDeltas: [],
			imageDeltas: [],
			plugins: [],
			languages: []
		}
	};

	// Ensure folder
	if (fs.existsSync(modPath))
		throw new Error(`Folder already exists at tomb/mods/${modId}`);
	fs.mkdirSync(modPath);

	// Generates all of the diffs. File writing happens in these functions
	json = diffData(json, modPath);
	json = diffLanguages(json, modPath);
	json = await diffImages(json, modPath);
	json = diffAudio(json, modPath);
	json = diffVideos(json, modPath);
	json = diffPlugins(json, modPath);

	// Delete any unused keys
	for (const _key in json.files) {
		const key = _key as keyof typeof json.files;
		if (Object.keys(json.files[key]).length === 0) delete json.files[key];
	}

	writeFile(path.join(modPath, 'mod.json'), JSON.stringify(json, null, '\t'));
}
