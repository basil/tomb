import fs from 'fs';
import path from 'path';
import fileList from './util/fileList.ts';
import { readFile, writeFile } from './util/fs.ts';
import { fromK9A } from '../../../../tomb/util.ts';

async function copyFolder(input: string, output: string) {
	console.time('copy');

	const files = fileList(input);
	// const progress = new Progress(files.length);
	// progress.show();
	// let status = 0;

	for (const file of files) {
		// progress.update(++status, file);
		const ext = path.extname(file);

		if (ext === '.k9a') {
			const newPath = fromK9A(file);
			const dec = readFile(path.join('www', file));

			writeFile(path.join(output, newPath), dec);

		} else if (ext === '.loc') {
			const rawData = fs.readFileSync(path.join('www', file));
			// Cut off the header
			const data = rawData.subarray(
				Buffer.byteLength(SIGNATURE, 'utf8') + 4,
				rawData.length + 4
			);

			// Format it
			const dataStr = JSON.parse(new TextDecoder().decode(data));
			const dataFmt = JSON.stringify(dataStr, null, '\t');

			writeFile(path.join(output, file), dataFmt);
		} else {
			const dec = readFile(path.join('www', file));

			writeFile(path.join(output, file), dec);
		}
	}

	// progress.remove();

	console.timeEnd('copy');
}

function generateRpgProject(output: string) {
	fs.writeFileSync(path.join(output, 'Game.rpgproject'), 'RPGMV 1.6.2');
}

function updatePackageJson(output: string) {
	const pkgPath = path.join(output, 'package.json');

	const rawPkg = fs.readFileSync(pkgPath, 'utf8');
	const pkg = JSON.parse(rawPkg);
	pkg.name = 'tcoaal';
	const newPkg = JSON.stringify(pkg, null, '    ');

	fs.writeFileSync(pkgPath, newPkg);
}

function patchIndexHtml(output: string) {
	// Need to find a good way to lint this...
	const script = `
		<script>
			// Some patches provided by Tomb

			// Patch language loading to support loading base-game language files without the header
			const orig = window.onload;
			window.onload = () => {
				const readFile = Utils.readFile;
				Utils.readFile = (arg) => {
					if (Utils.ext(arg) === '.loc') {
						// We pad the response with empty data, which the game cuts off
						return ' '.repeat(Buffer.byteLength(SIGNATURE, 'utf8') + 4)
							+ readFile(arg);
					}

					return readFile(arg);
				};

				if (Crypto.resolveURL) Crypto.resolveURL = url => url;

				orig();
			}
		</script>
	`.trim();

	const indexPath = path.join(output, 'index.html');
	const index = fs.readFileSync(indexPath, 'utf8');

	fs.writeFileSync(indexPath, index.replace('</body>', `${script}\n</body>`));
}

export default async function genProject() {
	const input = 'www';
	const output = 'project';

	fs.mkdirSync(output);

	// Progress bar doesn't work for some reason
	await copyFolder(input, output);
	generateRpgProject(output);
	updatePackageJson(output);
	patchIndexHtml(output);
}
