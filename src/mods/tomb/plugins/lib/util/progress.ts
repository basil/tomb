export default class Progress {
	readonly max: number;
	private _eContainer?: HTMLDivElement;
	private _eProgressBar?: HTMLDivElement;
	private _eStatusText?: HTMLSpanElement;

	constructor(max: number) {
		this.max = max;
	}

	show() {
		if (this._eContainer) throw new Error('Already visible');
		this._eContainer = $tomb.gui.e('div', {
			position: 'absolute',
			top: '12px',
			left: '0',
			right: '0',
			margin: 'auto',
			zIndex: '1',
			overflow: 'hidden',

			display: 'grid',
			placeItems: 'center',

			width: '200px',
			height: '40px',
			backgroundColor: '#3e070d',
			border: '1px solid #931b2b',
			borderRadius: '8px'
		});

		this._eProgressBar = $tomb.gui.e('div', {
			position: 'absolute',
			inset: '0',
			width: '0%',
			background: '#931b2b'
		});

		this._eStatusText = $tomb.gui.e('span', {
			position: 'relative',
			color: 'white',
			textShadow: '0 2px 4px black'
		});

		this._eContainer.append(this._eProgressBar);
		this._eContainer.append(this._eStatusText);

		document.body.append(this._eContainer);
	}

	remove() {
		this._eContainer.remove();
		this._eContainer = undefined;
		this._eProgressBar = undefined;
		this._eStatusText = undefined;
	}

	update(progress: number, text: string = undefined) {
		this._eProgressBar.style.width = `${(progress / this.max) * 100}%`;
		if (text) this._eStatusText.innerText = text;
	}
}
