import fs from 'fs';
import path from 'path';
import decrypt from './decrypt.ts';

/**
 * Reads a file, decrypting it if needed.
 * @param path The path to the file.
 * @returns
 */
export function readFile(path: string): Uint8Array {
	const data = fs.readFileSync(path);
	return decrypt(new Uint8Array(data), path);
}

/**
 * Makes sure a directory exists.
 * @param pathStr The path to the directory.
 */
export function ensureDir(pathStr: string) {
	let current = '';

	// Probably pretty hacky, but I couldn't find any native library function to do this.
	const segments = pathStr.split(path.sep);

	// Node.js versions before v10 don't support `mkdirSync`'s `recursive` attribute, so we have to loop over them ourselves
	for (const segment of segments) {
		current = path.join(current, segment);

		if (!fs.existsSync(current)) fs.mkdirSync(current);
	}
}

/**
 * Writes a file, making sure any parent directories exist.
 * @param filePath The path to the file.
 * @param data The data to write to the file.
 */
export function writeFile(filePath: string, data: string | Uint8Array) {
	ensureDir(path.dirname(filePath));
	fs.writeFileSync(filePath, data);
}

/**
 * Copies a file to another location, making any required folders.
 * @param src Source file.
 * @param dest Destination file.
 */
export function copyFile(src: string, dest: string) {
	ensureDir(path.dirname(dest));
	fs.copyFileSync(src, dest);
}
/**
 * Hashes the contents of a file with the game's built-in djb2 implementation (quick hashing function). Can only be ran after the game code is loaded.
 *
 * Decrypts files if needed.
 * @param filePath Path to the file .
 */
export function hashFile(filePath: string): Promise<number> {
	// @ts-expect-error No typings yet
	return Crypto.djb2(readFile(filePath));
}

/**
 * Checks if the hashes of a file are the same across the exported RPG Maker MV project and the original.
 *
 * Decrypts files if needed.
 * @param filePath Path to the file, relative to www/ and project/ (i.e. you would pass `data/Actors.json`)
 * @returns `true` if modified.
 */
export function isModified(filePath1: string, filePath2: string): boolean {
	return hashFile(filePath1) !== hashFile(filePath2);
}
