import fs from 'fs';
import path from 'path';

export default function fileList(folder: string, fragment = '') {
	const currentPath = path.join(folder, fragment);
	const entries = fs.readdirSync(currentPath);

	const files: string[] = [];

	for (const entry of entries) {
		if (entry === '.DS_Store') continue;

		const currentFile = path.join(currentPath, entry);
		const stats = fs.statSync(currentFile);

		if (stats.isFile()) {
			files.push(path.join(fragment, entry));
		} else if (stats.isDirectory()) {
			const subDir = fileList(folder, path.join(fragment, entry));
			for (const file of subDir) {
				files.push(file);
			}
		} else {
			throw new Error(`${currentFile} is not a file or a folder`);
		}
	}

	return files;
}
