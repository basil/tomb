import path from 'path';

const winToPosix = (winPath: string) =>
	winPath.split(path.sep).join(path.posix.sep);

export default winToPosix;
