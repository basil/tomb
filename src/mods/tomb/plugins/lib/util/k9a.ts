import fs from 'fs';
import { toK9A } from '../../../../../tomb/util.ts';

/** Converts a path to `.k9a` if needed. */
export const k9a = (wwwPath: string) => {
	const k9aPath = toK9A(wwwPath);

	if (fs.existsSync(k9aPath)) return k9aPath;
	return wwwPath;
};
