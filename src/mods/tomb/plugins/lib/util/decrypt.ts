export default function decrypt(rawUint: Uint8Array, filePath: string) {
	// @ts-expect-error These are not typed yet
	if (Crypto.dekit) return new Uint8Array(Crypto.dekit(rawUint, filePath, Crypto.guard()));

	// Fallback for older versions to avoid embedding personal information

	// @ts-expect-error This is not typed yet
	let mask: number = Crypto.mask(filePath);
	const signatureLength = SIGNATURE.length;
	const fileSignature = rawUint.slice(0, signatureLength);
	const expectedSignature = Array.from(SIGNATURE).map(
		(char) => (char.charCodeAt(0) ^ mask) % 256
	);

	// Checks if the expected signature is present at the top of every file
	if (fileSignature.toString() !== expectedSignature.toString()) {
		return rawUint;
	}

	// Seems to represent the length of the data
	let firstDataByte = rawUint[signatureLength];
	const data = rawUint.slice(signatureLength + 1);
	if (firstDataByte === 0) {
		firstDataByte = data.length;
	}
	for (let i = 0; i < firstDataByte; i++) {
		const byte = data[i];
		data[i] = (data[i] ^ mask) % 256;
		mask = (mask << 1) ^ byte;
	}

	return data;
}
