import type { ModJSON } from '../../../../../tomb/classes/mod.ts';

import path from 'path';
import fs from 'fs';
import { copyFile } from '../util/fs.ts';

type Plugin = {
	name: string;
	status: boolean;
	parameters: Record<string, string>;
};

function loadPlugin(path: string): Plugin[] {
	const data = fs.readFileSync(path, 'utf8');
	// Unsafe, but this doesn't seem like a pressing security vector.
	// The only thing I can think of is if a mod collaboration is happening, and one member puts malware here and has someone else bundle it.
	// But you can do the same in plugin files, so I don't think this is a special case we should care about
	const plugins: Plugin[] = new Function(`${data}\n; return $plugins;`)();
	return plugins;
}

/**
 * Sets up asset replacements for plugin files. Copies new plugins to the mod output, and adds them to the `ModJSON`.
 * @param json A `ModJSON` JSON object to use as the base for changes.
 * @returns The same `ModJSON`, but with the changes applies.
 */
export default function diffPlugins(json: ModJSON, modPath: string): ModJSON {
	const oldPlugins = loadPlugin('www/js/plugins.js');
	const newPlugins = loadPlugin('project/js/plugins.js');

	for (const plugin of newPlugins) {
		if (plugin.status === false) continue;
		if (oldPlugins.some(oldPlugin => plugin.name === oldPlugin.name)) continue;

		const file = `${plugin.name}.js`;

		copyFile(path.join('project', 'js', 'plugins', file), path.join(modPath, 'js', 'plugins', file));

		json.files.plugins.push(`js/plugins/${file}`);
	}

	return json;
}
