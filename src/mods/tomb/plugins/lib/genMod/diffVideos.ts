import type { ModJSON } from '../../../../../tomb/classes/mod.ts';

import path from 'path';
import fs, { existsSync } from 'fs';
import { copyFile, isModified } from '../util/fs.ts';
import fileList from '../util/fileList.ts';
import winToPosix from '../util/winToPosix.ts';
import { k9a } from '../util/k9a.ts';

/**
 * Sets up asset replacements for video files. Copies new video files to the mod output, and adds them to the `ModJSON`.
 * @param json A `ModJSON` JSON object to use as the base for changes.
 * @returns The same `ModJSON`, but with the changes applies.
 */
export default function diffVideos(json: ModJSON, modPath: string): ModJSON {
	if (!existsSync(path.join('project', 'movies'))) return json;

	const videos = fileList(path.join('project', 'movies'));

	for (const filePath of videos) {
		// Only check .ogg files
		if (!filePath.endsWith('.webm')) continue;

		const relativePath = path.join('movies', filePath);
		const origPath = k9a(path.join('www', relativePath));
		const newPath = path.join('project', relativePath);

		if (!fs.existsSync(origPath) || isModified(origPath, newPath)) {
			// We always fully replace video
			copyFile(newPath, path.join(modPath, relativePath));

			json.files.assets.push(winToPosix(relativePath));
		}
	}

	return json;
}
