import type { ModJSON } from '../../../../../tomb/classes/mod.ts';

import path from 'path';
import fs from 'fs';
import { copyFile, isModified, readFile, writeFile } from '../util/fs.ts';
import { computeDiff } from 'olid.ts';
import fileList from '../util/fileList.ts';
import winToPosix from '../util/winToPosix.ts';
import { k9a } from '../util/k9a.ts';

/**
 * Sets up patches/additions for image files. Copies new files, generates patches for existing files, and adds them to the `ModJSON`.
 * @param json A `ModJSON` JSON object to use as the base for changes.
 * @returns The same `ModJSON`, but with the changes applies.
 */
export default async function diffImages(
	json: ModJSON,
	modPath: string
): Promise<ModJSON> {
	// Images
	const folders = [
		'animations',
		'battlebacks1',
		'battlebacks2',
		'characters',
		'enemies',
		'faces',
		'parallaxes',
		'pictures',
		'sv_actors',
		'sv_enemies',
		'system',
		'tilesets',
		'titles1',
		'titles2'
	];

	for (const folder of folders) {
		if (!fs.existsSync(path.join('project/img', folder))) continue;

		const images = fileList(path.join('project/img', folder));

		for (const filePath of images) {
			// Only check .png files
			if (!filePath.endsWith('.png')) continue;

			const relativePath = path.join('img', folder, filePath);
			const origPath = k9a(path.join('www', relativePath));
			const newPath = path.join('project', relativePath);

			if (!fs.existsSync(origPath)) {
				// New asset

				copyFile(newPath, path.join(modPath, relativePath));

				json.files.assets.push(winToPosix(relativePath));
			} else if (isModified(origPath, newPath)) {
				// Patches

				const origFile = readFile(origPath);
				const newFile = readFile(newPath);

				const origImg = await createImageBitmap(new Blob([origFile]));
				const newImg = await createImageBitmap(new Blob([newFile]));

				const diff = computeDiff(origImg, newImg);

				const newRelativePath = path.join('img', folder, `${filePath}.olid`);

				json.files.imageDeltas.push(winToPosix(newRelativePath));

				writeFile(path.join(modPath, newRelativePath), new Uint8Array(diff));
			}
		}
	}

	return json;
}
