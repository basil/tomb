import type { ModJSON } from '../../../../../tomb/classes/mod.ts';

import fs from 'fs';
import path from 'path';
import { compare } from 'fast-json-patch';
import fileList from '../util/fileList.ts';
import { copyFile, isModified, readFile, writeFile } from '../util/fs.ts';
import winToPosix from '../util/winToPosix.ts';
import { k9a } from '../util/k9a.ts';

/**
 * Sets up patches/additions for data files. Copies new files, generates patches for existing files, and adds them to the `ModJSON`.
 * @param json A `ModJSON` JSON object to use as the base for changes.
 * @returns The same `ModJSON`, but with the changes applies.
 */
export default function diffData(json: ModJSON, modPath: string): ModJSON {
	const dataFiles = fileList('project/data');

	for (const _filePath of dataFiles) {
		const filePath = path.join('data', _filePath);
		// Only check .json files
		if (!filePath.endsWith('.json')) continue;

		const origPath = k9a(path.join('www', filePath));
		const newPath = path.join('project', filePath);

		if (!fs.existsSync(origPath)) {
			// New asset

			copyFile(newPath, path.join(modPath, filePath));

			json.files.assets.push(winToPosix(filePath));
		} else if (isModified(newPath, origPath)) {
			// Patches

			const decoder = new TextDecoder();

			const origFile = JSON.parse(decoder.decode(readFile(origPath)));
			const newFile = JSON.parse(decoder.decode(readFile(newPath)));

			// TODO: Ignorelist (ignore some changes that only appear in the RPG Maker MV editor, i.e. in MapInfos.json & System.json)
			const patch = compare(origFile, newFile);
			if (patch.length === 0) continue;

			const newRelativePath = filePath + 'd'; // .jsond

			json.files.dataDeltas.push(winToPosix(newRelativePath));

			writeFile(
				path.join(modPath, newRelativePath),
				JSON.stringify(patch, null, '\t')
			);
		}
	}

	return json;
}
