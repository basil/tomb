import type { ModJSON } from '../../../../../tomb/classes/mod.ts';

import path from 'path';
import fs from 'fs';
import { copyFile, isModified } from '../util/fs.ts';
import fileList from '../util/fileList.ts';
import winToPosix from '../util/winToPosix.ts';
import { k9a } from '../util/k9a.ts';

/**
 * Sets up asset replacements for audio files. Copies new and modified audio to the mod output, and adds them to the `ModJSON`.
 * @param json A `ModJSON` JSON object to use as the base for changes.
 * @returns The same `ModJSON`, but with the changes applies.
 */
export default function diffAudio(json: ModJSON, modPath: string): ModJSON {
	// Audio
	const folders = ['bgm', 'bgs', 'me', 'se'];

	for (const folder of folders) {
		if (!fs.existsSync(path.join('project/audio', folder))) continue;

		const images = fileList(path.join('project/audio', folder));

		for (const filePath of images) {
			// Only check .ogg files
			if (!filePath.endsWith('.ogg')) continue;

			const relativePath = path.join('audio', folder, filePath);
			const origPath = k9a(path.join('www', relativePath));
			const newPath = path.join('project', relativePath);

			if (!fs.existsSync(origPath) || isModified(origPath, newPath)) {
				// We always fully replace audio
				copyFile(newPath, path.join(modPath, relativePath));

				json.files.assets.push(winToPosix(relativePath));
			}
		}
	}

	return json;
}
