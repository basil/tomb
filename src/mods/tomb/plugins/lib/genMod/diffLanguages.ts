import type { ModJSON } from '../../../../../tomb/classes/mod.ts';
import type { LangData } from '../../../../../tomb/mods.ts';

import path from 'path';
import fs from 'fs';
import { isModified, writeFile } from '../util/fs.ts';
import winToPosix from '../util/winToPosix.ts';
import { k9a } from '../util/k9a.ts';

/**
 * Sets up patches for language files. Generates patches for existing languages and adds them to the `ModJSON`.
 * @param pathToFile Path to the file relative to the root of the game, i.e `www/languages/english/dialogue.loc`.
 * @returns
 */
function parseLanguageFile(pathToFile: string) {
	// Converts Windows-style slashes (\) to the slashes used in web requests (/)
	pathToFile = winToPosix(pathToFile);

	switch (path.extname(pathToFile).toLowerCase()) {
		case '.loc': {
			const data = fs.readFileSync(pathToFile);
			const signatureLength = Buffer.byteLength(SIGNATURE, 'utf8');
			const signature = data.slice(0, signatureLength).toString('utf8');
			const hasSignature = signature === SIGNATURE;
			const segment = hasSignature ? data.slice(signatureLength + 4) : data;
			const lang = JSON.parse(segment.toString('utf8'));

			return lang;
		}
		case '.txt':
			return $tomb.lib.lang.loadTXT(pathToFile);
		case '.csv':
			return $tomb.lib.lang.loadCSV(pathToFile);
		default:
			throw new Error('Failed to match language file extension to parser');
	}
}

export default function diffLanguages(json: ModJSON, modPath: string): ModJSON {
	// The sections we're checking for changes
	const sections: (keyof LangData)[] = [
		'sysLabel',
		'sysMenus',
		'labelLUT',
		'linesLUT'
	];

	const langDir = path.join(App.rootPath(), LANG_DIR);
	const langFolders = Utils.folders(langDir);

	// Iterate over every folder
	for (let i = 0; i < langFolders.length; i++) {
		const folder = path.join(langDir, langFolders[i]);
		const files = Utils.files(folder);

		// Iterate over every file in every folder
		for (const file of files) {
			// We only care about files we can parse (language files)
			const ext = path.extname(file).toLowerCase();
			if (!VALID_EXT.includes(ext)) continue;

			const filePath = path.join(LANG_DIR, langFolders[i], file);
			const origPath = k9a(path.join('www', filePath));
			const newPath = path.join('project', filePath);

			// Alert the developer if the original file doesn't exist? (we can't make a diff)
			// TODO: Also, maybe hold off on things like generating patches and writing new files until the very end of the diffing process, given that this error would abort the whole process and leave an unfinished mod. Or maybe just delete all of the files.
			if (!fs.existsSync(origPath))
				throw new Error(
					`Language file ${filePath} does not exist in www/, so we cannot generate a patched version`
				);

			// If the hashes differ, then the files are most likely different
			if (isModified(origPath, newPath)) {
				// Patches

				const origLangData = parseLanguageFile(origPath);
				const newLangData = parseLanguageFile(newPath);

				const diff: LangData = {};

				for (const section of sections) {
					for (const key in newLangData[section]) {
						if (
							JSON.stringify(origLangData[section][key]) !==
							JSON.stringify(newLangData[section][key])
						) {
							if (!diff[section]) diff[section] = {};
							diff[section][key] = newLangData[section][key];
						}
					}
				}

				// If the JSON object is empty, meaning that there was no different in actual content, then we don't generate a patch
				if (Object.keys(diff).length > 0) {
					const outputFile = path.join(LANG_DIR, `${langFolders[i]}.json`);

					json.files.languages.push(winToPosix(outputFile));

					writeFile(
						path.join(modPath, outputFile),
						JSON.stringify(diff, null, '\t')
					);
				}
			}
		}
	}

	return json;
}
