import type { BuildOptions } from 'esbuild';

import path from 'path';
import esbuild from 'esbuild';
import chokidar from 'chokidar';
// @ts-expect-error Since this is a script for newer Node versions and we have much older types installed for the game
import fs from 'fs/promises';
import Spinnies from 'spinnies';

const dev = process.argv[2] === 'dev';

const outDir = dev ? '../tomb' : 'out';
const out = (relative: string) => path.join(outDir, relative);

const env = process.env.TOMB_VERSION ? {
	'process.env.TOMB_VERSION': `'${process.env.TOMB_VERSION}'`,
	'process.env.TOMB_VERSION_FORMATTED': `'Tomb v${process.env.TOMB_VERSION}'`
} : {
	'process.env.TOMB_VERSION_FORMATTED': `'Tomb Dev Build'`
};

const esbuildConfig: BuildOptions = {
	define: env,
	dropLabels: dev ? [] : ['DEV'],
	bundle: true,
	target: 'chrome65',
	external: [
		'fs',
		'path',
		'zlib',
		'stream',
		'events',
		'util',
		'nw.gui',
		'./mods/*'
	]
};

const spinnies = new Spinnies();

async function ensureDir(path: string) {
	try {
		await fs.access(path);
	} catch {
		await fs.mkdir(path, { recursive: true });
	}
}

function watch(
	path: string,
	callback: (event: string, path?: string) => void | Promise<void>
) {
	if (dev) {
		chokidar
			.watch(path, { ignoreInitial: true })
			.on('all', callback)
			.on('ready', callback);
	} else callback('ready', undefined);
}

console.log(`Output directory: ${outDir}`);

await ensureDir(out('mods/tomb'));
await ensureDir(out('mods/tomb/img/system'));

// Tomb
watch('src/tomb', async () => {
	spinnies.add('tomb', { text: 'Building Tomb', status: 'spinning' });
	try {
		await esbuild.build({
			...esbuildConfig,
			entryPoints: ['src/tomb/index.ts'],
			outfile: out('tomb/tomb.js')
		});
		spinnies.succeed('tomb');
	} catch { spinnies.fail('tomb'); }
});

watch('src/index.html', async () => {
	spinnies.add('index.html', { text: 'Copying src/index.html' });
	await fs.copyFile('src/index.html', out('index.html'));
	spinnies.succeed('index.html');
});

// Tomb Core Mod
watch('src/mods/tomb', async (_, pathArg) => {
	async function copyFiles(pathArg: string | undefined, watch: string[]) {
		// `path` is undefined on first run (the `ready` event)
		if (!pathArg) {
			for (const file of watch) {
				spinnies.add(file, { text: `Copying ${file}` });
				await fs.copyFile(
					path.join('src/mods/tomb', file),
					out(path.join('mods/tomb', file))
				);
				spinnies.succeed(file);
			}
		}

		for (const file of watch) {
			if (pathArg === path.join('src/mods/tomb', file)) {
				spinnies.add(file, { text: `Copying ${file}` });
				await fs.copyFile(
					path.join('src/mods/tomb', file),
					out(path.join('mods/tomb', file))
				);
				spinnies.succeed(file);
				return true;
			}
		}

		return false;
	}

	const copied = await copyFiles(pathArg, [
		'img/system/wrench.png',
		'mod.json'
	]);
	if (copied) return;

	spinnies.add('tomb-core-mod', { text: 'Building Tomb Core Mod' });

	try {
		await esbuild.build({
			...esbuildConfig,
			entryPoints: ['src/mods/tomb/plugins/menu.ts'],
			outfile: out('mods/tomb/plugins/menu.js')
		});
		spinnies.succeed('tomb-core-mod');
	} catch { spinnies.fail('tomb-core-mod'); }
});
