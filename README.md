# Tomb

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

A mod loader for The Coffin of Andy and Leyley. Expect breaking changes. Draws from prior work in [OneLoader](https://github.com/rphsoftware/OneLoader) and [CCLoader](https://github.com/CCDirectLink/CCLoader).

## Getting Started

If you want to **make mods**, read the [Making Mods](https://coffin-wiki.basil.cafe/modding) section of the modding wiki.

If you want to **play mods**, read the [Playing Mods](https://coffin-wiki.basil.cafe/playing) section of the modding wiki.

## Developing Tomb

If you want to **work on Tomb**―for example, fixing a bug―refer to the following instructions. If you want to make a mod instead, please go to the [Getting Started](#getting-started) section.

Clone this repository into the same folder as the game, so the `tomb` folder is in the same directory as the `www` folder, and rename it to `tomb-src`:

```
The Coffin of Andy and Leyley/
├── tomb-src/
└── www/
```

Then, in the `tomb-src` folder, run:

```sh
pnpm i
pnpm dev
```

This will build Tomb in dev mode, which will reload the game every time you make a change to the source.

## TODO

- [ ] Allow mods to specify other mods as dependencies (i.e. library mods)
- [ ] Allow mods to specify other mods as conflicts
- [ ] Gracefully handle mod file conflicts (OneLoader-style)
- [ ] More descriptive error when no mod.json is present.
- [ ] Error handling papercuts
  - [ ] Handle multiple errors at once
- [ ] Logs?
- [ ] Refuse to load if coremod is missing?
- [ ] Add unit testing
- [ ] Drop object URLS once the network request consumes them.
